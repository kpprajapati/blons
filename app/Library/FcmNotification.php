<?php

namespace App\Library;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class FcmNotification
{
    public static function sendFCMNotification($tokens,$title,$bodyMsg,array $payloadData)
    {
        try {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            $notificationBuilder = new PayloadNotificationBuilder($title);
            $notificationBuilder->setBody($bodyMsg)
                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
//        $dataBuilder->addData(['a_data' => 'my_data']);
            $dataBuilder->addData($payloadData);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
            $response = $downstreamResponse->numberSuccess();
            return $response;
        }
        catch (Exception $e)
        {
            return 0;
        }
    }
}
