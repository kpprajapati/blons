<?php

namespace App\Library;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Exception\ImageException;
use Intervention\Image\ImageManagerStatic as Images;
use phpDocumentor\Reflection\Types\Self_;

class S3AwsFileUpload
{
    public static function _s3ImageUpload(Request $request, $folderName)
    {
        try {

            $attachment = $request->file('file');
            $imagePath = $attachment->getPathName();

            $s3 = Storage::disk('s3');

            $destinationPath = $folderName;
            $original_path = $destinationPath . '/original';
            $medium_path = $destinationPath . '/medium';
            $thumbnail_path = $destinationPath . '/thumbnail';

            $extension = $attachment->getClientOriginalExtension();
            $original_file_name = $attachment->getClientOriginalName();
            $fileName = Carbon::now()->format('YHisdm') . self::RandomString(33) . '.' . $extension;

            $oName = $original_path . '/' . $fileName;
            $mName = $medium_path . '/' . $fileName;
            $tName = $thumbnail_path . '/' . $fileName;

            $o = Storage::disk('s3')->put($original_path . '/' . $fileName, file_get_contents($attachment), 'public');
            $originalUrl = $s3->url($oName);

            if($extension == 'pdf' || $extension == 'PDF')
            {
                $m = true;
                $t = true;
                $mediumUrl = $originalUrl;
                $thumbnailUrl = $originalUrl;
            }
            else
            {
                $height = Images::make($attachment->getRealPath())->height();
                $width = Images::make($attachment->getRealPath())->width();

                $height_medium = !(empty($height)) ? ($height * 50 / 100) : 400;
                $width_medium = !(empty($width)) ? ($width * 50 / 100) : 400;

                $height_thumbnail = !(empty($height)) ? ($height * 25 / 100) : 200;
                $width_thumbnail = !(empty($width)) ? ($width * 20 / 100) : 200;

                $thumbnail = Images::make($attachment->getRealPath())->resize($width_thumbnail, $height_thumbnail)->stream();
                $t = Storage::disk('s3')->put($thumbnail_path . '/' . $fileName, (string)$thumbnail, 'public');
                $thumbnailUrl = $s3->url($tName);

                $medium = Images::make($attachment->getRealPath())->resize($width_medium, $height_medium)->stream();
                $m = Storage::disk('s3')->put($medium_path . '/' . $fileName, (string)$medium, 'public');
                $mediumUrl = $s3->url($mName);
            }


            return [
                'originalImageStatus' => $o,
                'mediumImageStatus' => $m,
                'thumbImageStatus' => $t,
                'originalImageURL' => $originalUrl,
                'mediumImageURL' => $mediumUrl,
                'thumbImageURL' => $thumbnailUrl,
                'fileName' => $fileName
            ];
        } catch (\Exception $e) {
            return [
                'originalImageStatus' => false,
                'mediumImageStatus' => false,
                'thumbImageStatus' => false,
                'originalImageURL' => null,
                'mediumImageURL' => null,
                'thumbImageURL' => null,
                'fileName' => null
            ];
        }
    }


    public static function _s3PropertyImageUpload($file, $folderName)
    {
        try {
            $attachment = $file;
            $imagePath = $attachment->getPathName();

            $s3 = Storage::disk('s3');

            $destinationPath = $folderName;
            $original_path = $destinationPath . '/original';
            $medium_path = $destinationPath . '/medium';
            $thumbnail_path = $destinationPath . '/thumbnail';

            $extension = $attachment->getClientOriginalExtension();
            $original_file_name = $attachment->getClientOriginalName();
            $fileName = Carbon::now()->format('YHisdm') . self::RandomString(33) . '.' . $extension;

            $oName = $original_path . '/' . $fileName;
            $mName = $medium_path . '/' . $fileName;
            $tName = $thumbnail_path . '/' . $fileName;

            $o = Storage::disk('s3')->put($original_path . '/' . $fileName, file_get_contents($attachment), 'public');
            $originalUrl = $s3->url($oName);

            if($extension == 'pdf' || $extension == 'PDF')
            {
                $m = true;
                $t = true;
                $mediumUrl = $originalUrl;
                $thumbnailUrl = $originalUrl;
            }
            else
            {
                $height = Images::make($attachment->getRealPath())->height();
                $width = Images::make($attachment->getRealPath())->width();

                $height_medium = !(empty($height)) ? ($height * 50 / 100) : 400;
                $width_medium = !(empty($width)) ? ($width * 50 / 100) : 400;

                $height_thumbnail = !(empty($height)) ? ($height * 25 / 100) : 200;
                $width_thumbnail = !(empty($width)) ? ($width * 20 / 100) : 200;

                $thumbnail = Images::make($attachment->getRealPath())->resize($width_thumbnail, $height_thumbnail)->stream();
                $t = Storage::disk('s3')->put($thumbnail_path . '/' . $fileName, (string)$thumbnail, 'public');
                $thumbnailUrl = $s3->url($tName);

                $medium = Images::make($attachment->getRealPath())->resize($width_medium, $height_medium)->stream();
                $m = Storage::disk('s3')->put($medium_path . '/' . $fileName, (string)$medium, 'public');
                $mediumUrl = $s3->url($mName);
            }

            return [
                'originalImageStatus' => $o,
                'mediumImageStatus' => $m,
                'thumbImageStatus' => $t,
                'originalImageURL' => $originalUrl,
                'mediumImageURL' => $mediumUrl,
                'thumbImageURL' => $thumbnailUrl,
                'fileName' => $fileName
            ];
        } catch (\Exception $e) {
            return [
                'originalImageStatus' => false,
                'mediumImageStatus' => false,
                'thumbImageStatus' => false,
                'originalImageURL' => null,
                'mediumImageURL' => null,
                'thumbImageURL' => null,
                'fileName' => null
            ];
        }
    }

    protected static function RandomString($length)
    {
        $keys = array_merge(range(0, 9), range('a', 'z'));

        $key = "";
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[mt_rand(0, count($keys) - 1)];
        }
        return $key.rand(1111111111,9999999999);
    }
}

?>
