<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    const ACTIVE = 1, IN_ACTIVE = 0;
    const TEXTBOX = 1, TEXTAREA = 2;

    protected $fillable = [
        'question',
        'loan_type_id',
        'input_type',
        'status'
    ];

    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function loanType()
    {
        return $this->belongsTo(LoanType::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('questions.status',self::ACTIVE);
    }

    public function scopeTextBox($query)
    {
        return $query->where('questions.input_type',self::TEXTBOX);
    }

    public function scopeTextArea($query)
    {
        return $query->where('questions.input_type',self::TEXTAREA);
    }

}
