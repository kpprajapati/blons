<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanRequest extends Model
{
    use SoftDeletes;

    const PENDING = 0, ACCEPT = 1;

    protected $fillable = [
        'request_number',
        'user_id',
        'bo_name',
        'bo_email',
        'bo_phone',
        'bo_address',
        'bo_postal_code',
        'bo_dob',
        'credit_score',
        'address',
        'current_value',
        'expected_value',
        'year_built',
        'sq_fit',
        'number_limits',
        'category_id',
        'property_type_id',
        'loan_type_id',
        'purchase_price',
        'amount_of_loan_request',
        'loan_terms',
        'ask',
        'dp_percentage',
        'net_amount',
        'status',
    ];

    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @return mixed
     */

    public function scopePending($query)
    {
        return $query->where('loan_requests.status',self::PENDING);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function propertyType()
    {
        return $this->belongsTo(PropertyType::class);
    }

    public function loanType()
    {
        return $this->belongsTo(LoanType::class);
    }

    public function extraInformation()
    {
        return $this->hasMany(ExtraInformation::class);
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }


}
