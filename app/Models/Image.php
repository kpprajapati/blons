<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'original_url',
        'medium_url',
        'thumb_url'
    ];

    protected $dates = ['deleted_at'];

}
