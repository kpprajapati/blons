<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attachment extends Model
{
    use SoftDeletes;

    const IMAGE = 1, OTHER = 2;

    protected $fillable = [
        'loan_request_id',
        'name',
        'original_url',
        'medium_url',
        'thumb_url',
        'doc_type',
    ];

    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeImage($query)
    {
        return $query->where('attachments.doc_type',self::IMAGE);
    }

    public function scopeOther($query)
    {
        return $query->where('attachments.doc_type',self::OTHER);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function loanRequest()
    {
        return $this->belongsTo(LoanRequest::class);
    }
}
