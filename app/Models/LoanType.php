<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanType extends Model
{
    use SoftDeletes;

    const INACTIVE = 0, ACTIVE = 1;

    protected $fillable = [
        'name',
        'description',
        'status'
    ];

    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('loan_types.status',self::ACTIVE);
    }

}
