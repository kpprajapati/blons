<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyType extends Model
{
    use SoftDeletes;

    const INACTIVE = 0, ACTIVE = 1;

    protected $fillable = [
        'name',
        'image_id',
        'description',
        'status'
    ];

    protected $dates = ['deleted_at'];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('property_types.status',self::ACTIVE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->belongsTo(Image::class);
    }
}
