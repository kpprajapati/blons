<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExtraInformation extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'loan_request_id',
        'question',
        'answer'
    ];

    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function loanRequest()
    {
        return $this->belongsTo(LoanRequest::class);
    }
}
