<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\CsvData
 *
 * @property int $id
 * @property int|null $zip_code
 * @property string|null $place_name
 * @property string|null $state
 * @property string|null $state_abb
 * @property string|null $state_code
 * @property string|null $country
 * @property string|null $lat
 * @property string|null $lng
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvData onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData wherePlaceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData whereStateAbb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData whereStateCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CsvData whereZipCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvData withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvData withoutTrashed()
 * @mixin \Eloquent
 */
class CsvData extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'zip_code',
        'place_name',
        'state',
        'state_code',
        'lat',
        'lng'
    ];

    protected $dates = ['deleted_at'];
}
