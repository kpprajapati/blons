<?php

namespace App\Http\Middleware;

use Closure;

class AgentAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
        if(!$user)
        {
             return redirect(route('frontend.home.index'));
        }

        if(!$user->hasRole('agent'))
        {
            return redirect(route('frontend.home.index'));
        }

        return $next($request);
    }
}
