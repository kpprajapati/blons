<?php

namespace App\Http\Middleware;

use Closure;

class CustomerAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
        if(!$user)
        {
            return redirect(route('frontend.login.index'));
        }

        if(!$user->hasRole('customer'))
        {
            return redirect(route('frontend.login.index'));
        }

        return $next($request);
    }
}
