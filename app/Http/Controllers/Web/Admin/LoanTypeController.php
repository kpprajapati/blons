<?php

namespace App\Http\Controllers\Web\Admin;

use App\Library\S3AwsFileUpload;
use App\Models\LoanType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanTypeController extends Controller
{
    public function index()
    {
        return view('admin.loanType.index',[
            'types' => LoanType::all()
        ]);
    }

    public function add()
    {
        return view('admin.loanType.add');
    }

    public function create(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:loan_types,name',
            'description' => 'required'
        ]);

        $obj = new LoanType();
        $obj->create([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'status' => $request->get('status') == 1 ? LoanType::ACTIVE : LoanType::INACTIVE
        ]);

        return redirect(route('admin.loanType.index'))->with(['success' => 'Successfully created !']);
    }

    public function edit($id)
    {
        if(!$type = LoanType::find($id))
        {
            return redirect(route('admin.loanType.index'))->with(['error' => 'Invalid id selected']);
        }

        return view('admin.loanType.edit',[
            'type' => $type
        ]);
    }

    public function update($id,Request $request)
    {
        $this->validate($request, [
            'name' =>'required'
        ]);

        if(!$type = LoanType::find($id))
        {
            return redirect(route('admin.loanType.index'))->with(['error' => 'Invalid id selected']);
        }

        $type->name = $request->get('name');
        $type->description = $request->get('description');
        $type->status = $request->get('status') == 1 ? LoanType::ACTIVE : LoanType::INACTIVE;
        $type->save();

        return redirect(route('admin.loanType.index'))->with(['success' => 'Successfully updated']);
    }
}
