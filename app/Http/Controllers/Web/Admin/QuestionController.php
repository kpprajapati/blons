<?php

namespace App\Http\Controllers\Web\Admin;

use App\Models\LoanType;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{
    public function index()
    {
        return view('admin.question.index',[
            'questions' => Question::with(['loanType'])->get()
        ]);
    }

    public function add()
    {
        return view('admin.question.add',[
            'loanTypes' => LoanType::active()->get()
        ]);
    }

    public function create(Request $request)
    {

        $this->validate($request, [
            'question' => 'required|unique:questions,question',
            'loanType' => 'required|exists:loan_types,id',
            'inputType' => 'required|in:1,2'
        ]);

        $obj = new Question();
        $obj->create([
            'question' => $request->get('question'),
            'loan_type_id' => $request->get('loanType'),
            'status' => $request->get('status') == 1 ? Question::ACTIVE : Question::IN_ACTIVE,
            'input_type' => $request->get('inputType') == 1 ? Question::TEXTBOX : Question::TEXTAREA
        ]);

        return redirect(route('admin.question.index'))->with(['success' => 'Successfully created !']);
    }

    public function edit($id)
    {
        if(!$question = Question::find($id))
        {
            return redirect(route('admin.question.index'))->with(['error' => 'Invalid id selected']);
        }

        return view('admin.question.edit',[
            'question' => $question,
            'loanTypes' => LoanType::active()->get()
        ]);
    }

    public function update($id,Request $request)
    {
        $this->validate($request, [
            'question' =>'required'
        ]);

        if(!$question = Question::find($id))
        {
            return redirect(route('admin.question.index'))->with(['error' => 'Invalid id selected']);
        }

        $question->question = $request->get('question');
        $question->loan_type_id = $request->get('loanType');
        $question->input_type = $request->get('inputType') == 1 ? Question::TEXTBOX : Question::TEXTAREA;
        $question->status = $request->get('status') == 1 ? Question::ACTIVE : Question::IN_ACTIVE;
        $question->save();

        return redirect(route('admin.question.index'))->with(['success' => 'Successfully updated']);
    }
}
