<?php

namespace App\Http\Controllers\Web\Admin;

use App\Models\Attachment;
use App\Models\Inquiry;
use App\Models\LoanRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InquiryController extends Controller
{
    public function index()
    {
        return view('admin.inquiry.index',[
            'inquiries' => LoanRequest::orderBy('id','DESC')->paginate(10)
        ]);
    }

    public function detail($id)
    {
        if(!$inquiry = LoanRequest::with(['user','loanType','propertyType','extraInformation'])->find($id))
        {
            return redirect(route('admin.inquiry.index'))->with(['error' => 'Invalid id selected']);
        }

        return view('admin.inquiry.detail',[
            'inc' => $inquiry
        ]);
    }


}
