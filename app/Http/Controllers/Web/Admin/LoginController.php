<?php

namespace App\Http\Controllers\Web\Admin;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function index()
    {
        return view('admin.login.index');
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($user = User::where('email',$request->get('email'))->active()->first())
        {
            if(Hash::check($request->get('password'),$user->password))
            {
                if ($user->hasRole('admin')) {
                    \Auth::login($user);
                    return redirect(route('admin.dashboard.index'));
                }
                else
                {
//                    dd('role not found');
                    return redirect(route('admin.login.index'))->with(['error' => 'Unauthenticated user!']);
                }
//                \Auth::login($user);
            }
        }
        return redirect(route('admin.login.index'))->with(['error' => 'Invalid credential try again !']);
    }

    public function logout(Request $request)
    {
        \Auth::logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect(route('admin.login.index'));
    }
}
