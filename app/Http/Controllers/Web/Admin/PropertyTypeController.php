<?php

namespace App\Http\Controllers\Web\Admin;

use App\Library\FcmNotification;
use App\Library\S3AwsFileUpload;
use App\Models\Image;
use App\Models\PropertyType;
use App\Models\CsvData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use FCM;

class PropertyTypeController extends Controller
{
    public function index()
    {
        return view('admin.propertyType.index',[
            'types' => PropertyType::with(['image'])->get()
        ]);
    }

    public function add()
    {
        return view('admin.propertyType.add');
    }

    public function create(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:property_types,name',
            'file' => 'required|image',
            'description' => 'required'
        ]);

        $image = null;

        $response = S3AwsFileUpload::_s3ImageUpload($request,'PropertyImages');
        if($response['originalImageStatus']) {

            $imObj = new Image();
            $image = $imObj->create([
               'name' =>  $response['fileName'],
                'original_url' => $response['originalImageURL'],
                'medium_url' => $response['mediumImageURL'],
                'thumb_url' => $response['thumbImageURL'],
            ]);
        }

        $obj = new PropertyType();
        $obj->create([
            'name' => $request->get('name'),
            'image_id' => $image ? $image->id : null,
            'description' => $request->get('description'),
            'status' => $request->get('status') == 1 ? PropertyType::ACTIVE : PropertyType::INACTIVE
        ]);

        return redirect(route('admin.propertyType.index'))->with(['success' => 'Successfully created !']);
    }

    public function edit($id)
    {
        if(!$type = PropertyType::find($id))
        {
            return redirect(route('admin.propertyType.index'))->with(['error' => 'Invalid id selected']);
        }

        return view('admin.propertyType.edit',[
            'type' => $type
        ]);
    }

    public function update($id,Request $request)
    {
        $this->validate($request, [
            'name' =>'required'
        ]);

        if(!$type = PropertyType::with(['image'])->find($id))
        {
            return redirect(route('admin.propertyType.index'))->with(['error' => 'Invalid id selected']);
        }

        if($request->has('file'))
        {
                $response = S3AwsFileUpload::_s3ImageUpload($request,'PropertyImages');
                if($response['originalImageStatus']) {
                    $image = Image::find($type->image_id);
                    $image->name =  $response['fileName'];
                    $image->original_url = $response['originalImageURL'];
                    $image->medium_url = $response['mediumImageURL'];
                    $image->thumb_url = $response['thumbImageURL'];
                    $image->save();
                }
        }

        $type->name = $request->get('name');
        $type->description = $request->get('description');
        $type->status = $request->get('status') == 1 ? PropertyType::ACTIVE : PropertyType::INACTIVE;
        $type->save();

        return redirect(route('admin.propertyType.index'))->with(['success' => 'Successfully updated']);
    }
}
