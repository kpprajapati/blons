<?php

namespace App\Http\Controllers\Web\Admin\Api;

use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class subCategoryController extends Controller
{
    public function getSubCategoryFromCategory(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|exists:categories,id'
        ]);

        return response()->json([
            "subCategories" => SubCategory::where('category_id',$request->get('category_id'))->active()->get()
        ]);
    }
}
