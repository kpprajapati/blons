<?php

namespace App\Http\Controllers\Web\Admin\Api;

use App\Models\LoanType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class loanTypeController extends Controller
{
    public function getLoanTypesFromSubCategory(Request $request)
    {
        $this->validate($request, [
            'sub_category_id' => 'required|exists:sub_categories,id'
        ]);

        return response()->json([
            "loanTypes" => LoanType::where('sub_category_id',$request->get('sub_category_id'))->active()->get()
        ]);
    }
}
