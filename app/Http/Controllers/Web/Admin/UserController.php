<?php

namespace App\Http\Controllers\Web\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        return view('admin.user.index',[
            'userLists' => User::paginate(2)
        ]);
    }

    public function detail($id)
    {
        if(!$user = User::with(['loanRequests'])->find($id))
        {
            return redirect(route('admin.user.index'))->with(['error' => 'Invalid id selected']);
        }

        return view('admin.user.detail',[
            'user' => $user
        ]);
    }
}
