<?php

namespace App\Http\Controllers\Web\Frontend;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function  index()
    {
        return view('frontend.login.index');
    }

    public function signIn(Request $request)
    {
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        if($user = User::where('email',$request->get('email'))->first())
        {
            if($user->hasRole('customer'))
            {
                if(Hash::check($request->get('password'),$user->password))
                {
                    \Auth::login($user);
                    return redirect(route('frontend.home.index'));
                }
            }
        }
        return redirect(route('frontend.login.index'))->with(['error' => 'Invalid credential please Try again !']);
    }

    public function logout(Request $request)
    {
        \Auth::logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect(route('frontend.home.index'));
    }

    public function  forgotPassword()
    {
        return view('frontend.login.forgotPassword');
    }

    public function  forgotPasswordEmail(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email|exists:users,email',
        ],[
            'email.exists' => 'Please enter registered email'
        ]);

//        try
//        {
//            $user = User::where('email',$request->get('email'))->first();
//            $key = $this->RandomString(50).date('dym').rand(0000,9999);
//            $user->fo_password_key = $key;
//            $user->save();
//            $link = env('APP_URL')."reset/".$key;
//            Mail::to($user->email)->send(new ResetPasswordEamil($link));
//        }
//        catch (\Exception $e)
//        {
//            return redirect(route('frontend.login.forgotPassword'))->with(['error' => 'Something went wrong. Please try again later !']);
//        }

        return redirect(route('frontend.login.forgotPassword'))->with(['success' => 'Email successfully send']);
    }
}
