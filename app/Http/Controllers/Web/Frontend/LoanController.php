<?php

namespace App\Http\Controllers\Web\Frontend;

use App\Models\LoanRequest;
use App\Models\LoanType;
use App\Models\PropertyType;
use Illuminate\Http\Request;

class LoanController extends FrontendController
{
    public function index()
    {
        return view('frontend.loan.index',[
            'loanTypes' => LoanType::active()->get(),
            'propertyTypes' => PropertyType::active()->get()
        ]);
    }

    public function requestList()
    {
        return view('frontend.loan.requestList',[
            'requests' => LoanRequest::where('user_id',$this->user->id)->orderBy('id','DESC')->paginate(5)
        ]);
    }


    public function detail($id)
    {
        if(!$inquiry = LoanRequest::with(['user','loanType','propertyType','extraInformation','attachments'])->find($id))
        {
            return redirect(route('frontend.loan.requestList'))->with(['error' => 'Invalid id selected']);
        }

        return view('frontend.loan.detail',[
            'inc' => $inquiry
        ]);
    }
}
