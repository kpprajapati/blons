<?php

namespace App\Http\Controllers\Web\Frontend;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class RegisterController extends Controller
{
    public function index()
    {
        return view('frontend.register.index');
    }

    public function signUp(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
            'mobile' => 'required|unique:users,mobile'
        ],[
            'name.required' => 'Name is required',
            'email.required' => 'Email is required',
            'email.email' => 'Please enter valid email',
            'email.unique' => 'Email already exists',
            'password.required' => 'Password is required',
            'confirm_password.required' => 'Confirm Password is required',
            'confirm_password.same' => 'Confirm Password does not matched with password',
        ]);

        $response = false;

        try
        {
            $response = \DB::transaction(function () use ($request){
                $userModel = new User();
                $user = $userModel->create([
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'mobile' => $request->get('mobile'),
                    'password' => bcrypt($request->get('confirm_password')),
                    'status' => User::ACTIVE
                ]);

//                if($request->get('type') == 1)
//                {
//                    $user->attachRole(Role::name('agent')->first());
//
////                    $agentModel = new Agent();
////                    $agentModel->create([
////                        'user_id' => $user->id
////                    ]);
//                }
//                else
//                {
//
////                    $customerModel = new Customer();
////                    $customerModel->create([
////                        'user_id' => $user->id
////                    ]);
//                }

                $user->attachRole(Role::name('customer')->first());
                \Auth::login($user);
                return true;
            });
        }
        catch (\Exception $e)
        {
            return redirect(route('frontend.register.index'))->with(['error' => 'Something went wrong ! Please try again later.']);
        }

        if($response == true)
        {
            return redirect(route('frontend.home.index'))->with(['success' => 'Welcome to basrow - loan.']);
        }

    }


}
