<?php

namespace App\Http\Controllers\Web\Frontend\Api;

use App\Events\LoanRequestEvent;
use App\Http\Controllers\Web\Frontend\FrontendController;
use App\Models\Attachment;
use App\Models\ExtraInformation;
use App\Models\Inquiry;
use App\Models\LoanRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LoanController extends FrontendController
{
    public function sendLoanRequest(Request $request)
    {
        $this->validate($request, [
            'bo_name' => 'required',
            'bo_email' => 'required',
            'bo_phone' => 'required',
            'credit_score' => 'required',
            'bo_dob' => 'required|date'
        ]);

        try {

            $reqNumber = \DB::transaction(function () use ($request) {

                $req = new LoanRequest();
                $loanRequest = $req->create([
                    'user_id' => $this->user->id,
                    'bo_name' => $request->get('bo_name'),
                    'bo_email' => $request->get('bo_email'),
                    'bo_phone' => $request->get('bo_phone'),
                    'bo_address' => $request->get('bo_address'),
                    'bo_postal_code' => $request->get('bo_postal_code'),
                    'bo_dob' => Carbon::parse($request->get('bo_dob'))->format('Y-m-d'),
                    'credit_score' => $request->get('credit_score'),
                    'address' => $request->get('address'),
                    'current_value' => $request->get('current_value'),
                    'expected_value' => $request->get('expected_value'),
                    'year_built' => $request->get('year_built'),
                    'sq_fit' => $request->get('sq_fit'),
                    'number_limits' => $request->get('number_limits'),
                    'property_type_id' => $request->get('property_type'),
                    'loan_type_id' => $request->get('loan_type'),
                    'purchase_price' => $request->get('purchase_price'),
                    'amount_of_loan_request' => $request->get('amount_of_loan_request'),
                    'loan_terms' => $request->get('loan_terms'),
                    'ask' => $request->get('ask'),
                    'dp_percentage' => $request->get('dp_percentage'),
                    'net_amount' => $request->get('net_amount'),
                ]);

                $reqNumber = "#Request-" . $loanRequest->id;
                $loanRequest->request_number = $reqNumber;
                $loanRequest->save();

                $imageArray = $request->get('imageArray');
                if (count($imageArray) > 0) {
                    for ($i = 0; $i < count($imageArray); $i++) {
                        $propImagesModel = new Attachment();
                        $propImagesModel->create([
                            'name' => $imageArray[$i]['fileName'],
                            'original_url' => $imageArray[$i]['originalUrl'],
                            'medium_url' => $imageArray[$i]['mediumUrl'],
                            'thumb_url' => $imageArray[$i]['thumbUrl'],
                            'loan_request_id' => $loanRequest->id,
                            'doc_type' => $imageArray[$i]['types'],
                        ]);
                    }
                }

                $extraArray = $request->get('extra');
                if (count($extraArray) > 0) {
                    for ($i = 0; $i < count($extraArray); $i++) {
                        $extraModel = new ExtraInformation();
                        $extraModel->create([
                            'question' => $extraArray[$i]['question'],
                            'answer' => $extraArray[$i]['answer'],
                            'loan_request_id' => $loanRequest->id
                        ]);
                    }
                }

//                Mail::to(env('ADMIN_EMAIL'))->send(new \App\Mail\LoanRequest($loanRequest->id));

                event(new LoanRequestEvent('123'));

                return $reqNumber;

            }); // end of transaction

        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => 'Something went wrong'
            ];
        }

        return [
            'status' => true,
            'message' => 'request successfully sent and your request number is : ' . $reqNumber
        ];
    }
}
