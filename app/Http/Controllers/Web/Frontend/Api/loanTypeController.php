<?php

namespace App\Http\Controllers\Web\Frontend\Api;

use App\Models\LoanType;
use App\Models\SubLoanType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class loanTypeController extends Controller
{
    public function getLoanTypesFromSubCategory(Request $request)
    {
        $this->validate($request, [
            'sub_category_id' => 'required|exists:sub_categories,id'
        ]);

        return response()->json([
            "loanTypes" => LoanType::where('sub_category_id',$request->get('sub_category_id'))->active()->get()
        ]);
    }

    public function getSubLoanTypesFromLoanType(Request $request)
    {
        $this->validate($request, [
            'loan_type_id' => 'required|exists:sub_categories,id'
        ]);

        return response()->json([
            "subLoanTypes" => SubLoanType::where('loan_type_id',$request->get('loan_type_id'))->active()->get()
        ]);
    }
}
