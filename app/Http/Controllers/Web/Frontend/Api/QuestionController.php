<?php

namespace App\Http\Controllers\Web\Frontend\Api;

use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{
    public function getQuestionsFormLoanType(Request $request)
    {
        $this->validate($request, [
           'loan_type_id' => 'required|exists:loan_types,id'
        ]);

        $questions = Question::where('loan_type_id',$request->get('loan_type_id'))->get();

        return [
            'questions' => $questions
        ];
    }
}
