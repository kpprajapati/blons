<?php

namespace App\Http\Controllers\Web\Frontend\Api;

use App\Library\S3AwsFileUpload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    public function attachDoc(Request $request)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);

        try {
            $file = $request->file('file');
            if ($file->getClientOriginalExtension() == 'jpg' ||
                $file->getClientOriginalExtension() == 'jpeg' ||
                $file->getClientOriginalExtension() == 'png' ||
                $file->getClientOriginalExtension() == 'pdf' ||
                $file->getClientOriginalExtension() == 'PDF' ||
                $file->getClientOriginalExtension() == 'bmp')
            {
                $response = S3AwsFileUpload::_s3PropertyImageUpload($request->file('file'),'LoanRequest');
                if($response['originalImageStatus']) {
                    return response()->json([
                        'status' => true,
                        'image_name' => $response['fileName'],
                        'original_url' => $response['originalImageURL'],
                        'medium_url' => $response['mediumImageURL'],
                        'thumb_url' => $response['thumbImageURL'],
                        'code' => rand(00000, 99999)
                    ]);
                }
                else {
                    return response()->json([
                        'status' => false,
                        'image_name' => null,
                        'original_url' => null,
                        'medium_url' => null,
                        'thumb_url' => null,
                        'code' => null
                    ]);
                }

            } else {
                return response()->json([
                    'status' => false,
                    'image_name' => null,
                    'original_url' => null,
                    'medium_url' => null,
                    'thumb_url' => null,
                    'code' => null
                ]);
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'image_name' => null,
                'original_url' => null,
                'medium_url' => null,
                'thumb_url' => null,
                'code' => null
            ]);
        }

    }
}
