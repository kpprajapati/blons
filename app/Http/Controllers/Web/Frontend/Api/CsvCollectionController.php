<?php

namespace App\Http\Controllers\Web\Frontend\Api;

use App\Models\CsvData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CsvCollectionController extends Controller
{
    public function search(Request $request)
    {
        $this->validate($request,[
            'search' => 'required'
        ]);

        $csvResponseData = CsvData::where('zip_code', 'LIKE',"%".$request->get('search')."%")
            ->orWhere('place_name', 'LIKE', "%".$request->get('search')."%")
            ->orWhere('state', 'LIKE', "%".$request->get('search')."%")
            ->orWhere('state_code', 'LIKE', "%".$request->get('search')."%")
            ->limit(5)
            ->get();

        if(is_numeric(trim($request->get('search'))))
        {
            return response()->json(
                $csvResponseData->map(function ($response){
                    return [
                        'postal_code' => $response->zip_code,
                        'place_name' => $response->place_name,
                        'state' => $response->state,
                        'state_code' => $response->state_code,
                    ];
                })
            );
        }
        else
        {
            return response()->json(
                $csvResponseData->map(function ($response){
                    return [
                        'postal_code' => $response->place_name,
                        'place_name' => $response->zip_code,
                        'state' => $response->state,
                        'state_code' => $response->state_code,
                    ];
                })
            );
        }

    }

    public function searchAddressDetails(Request $request)
    {
        $this->validate($request,[
            'search' => 'required'
        ]);

        $csvResponseData = CsvData::where('zip_code', 'LIKE',"%".$request->get('search')."%")
            ->orWhere('place_name', 'LIKE', "%".$request->get('search')."%")
            ->orWhere('state', 'LIKE', "%".$request->get('search')."%")
            ->orWhere('state_code', 'LIKE', "%".$request->get('search')."%")
            ->limit(5)
            ->get();

            return response()->json(
                $csvResponseData->map(function ($response){
                    return [
                        'postal_code' => $response->zip_code,
                        'place_name' => $response->place_name,
                        'city' => $response->place_name,
                        'state' => $response->state,
                        'state_code' => $response->state_code,
                        'country' => $response->country,
                        'lat' => $response->lat,
                        'lng' => $response->lng1
                    ];
                })
            );
    }

    public function searchServiceArea(Request $request)
    {
        $this->validate($request,[
            'search' => 'required'
        ]);

        $csvResponseData = CsvData::where('zip_code', 'LIKE',"%".$request->get('search')."%")
            ->limit(5)
            ->get();

        return response()->json(
            $csvResponseData->map(function ($response){
                return [
                    'postal_code' => $response->zip_code,
                    'place_name' => $response->place_name,
                    'city' => $response->place_name,
                    'state' => $response->state,
                    'state_code' => $response->state_code,
                    'country' => $response->country,
                    'lat' => $response->lat,
                    'lng' => $response->lng
                ];
            })
        );
    }
}
