<?php

namespace App\Http\Controllers\Web\Frontend;

use App\Models\Code;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Storage;
//use Intervention\Image\Exception\ImageException;
//use Intervention\Image\ImageManagerStatic as Images;

class FrontendController extends Controller
{
    public $user = null;
    public function __construct(Request $request)
    {
        $this->middleware(function ($request, $next) {
            if (\Auth::user()) {
                $this->user = \Auth::user();
            }
            return $next($request);
        });
    }

    function RandomString($length) {
        $keys = array_merge(range(0,9), range('a', 'z'));

        $key = "";
        for($i=0; $i < $length; $i++) {
            $key .= $keys[mt_rand(0, count($keys) - 1)];
        }
        return $key;
    }

    public function getStatusCode($code_param)
    {
        $response = Code::where('code',$code_param)->first();
        return $response;
    }


//    public function _s3ImageUpload($request,$folderName)
//    {
//        $attachment = $request->file('file');
//        $imagePath = $attachment->getPathName();
//
//        $s3 = Storage::disk('s3');
//
//        $destinationPath    = $folderName;
//        $original_path = $destinationPath . '/original';
//        $medium_path = $destinationPath . '/medium';
//        $thumbnail_path = $destinationPath . '/thumbnail';
//
//        $extension = $attachment->getClientOriginalExtension();
//        $original_file_name = $attachment->getClientOriginalName();
//        $new_name = time() . rand(11111, 99999);
//        $fileName = $new_name . '.' . $extension;
//
//        $oName = $original_path.'/'.$fileName;
//        $mName = $medium_path.'/'.$fileName;
//        $tName = $thumbnail_path.'/'.$fileName;
//
//        $o = Storage::disk('s3')->put($original_path.'/'.$fileName, file_get_contents($attachment), 'public');
//        $height = Images::make($attachment->getRealPath())->height();
//        $width = Images::make($attachment->getRealPath())->width();
//        $originalUrl = $s3->url($oName);
//
//        $height_medium = !(empty($height)) ? ($height * 50 / 100) : 400;
//        $width_medium = !(empty($width)) ? ($width * 50 / 100) : 400;
//
//        $height_thumbnail = !(empty($height)) ? ($height * 25 / 100) : 200;
//        $width_thumbnail = !(empty($width)) ? ($width * 20 / 100) : 200;
//
//        $thumbnail = Images::make($attachment->getRealPath())->resize($width_thumbnail, $height_thumbnail)->stream();
//        $t = Storage::disk('s3')->put($thumbnail_path . '/' . $fileName, (string)$thumbnail, 'public');
//        $thumbnailUrl = $s3->url($tName);
//
//        $medium = Images::make($attachment->getRealPath())->resize($width_medium, $height_medium)->stream();
//        $m = Storage::disk('s3')->put($medium_path . '/' . $fileName, (string)$medium, 'public');
//        $mediumUrl = $s3->url($mName);
//
//       return [
//            'original' => $originalUrl,
//            'medium' => $mediumUrl,
//            'thumb' => $thumbnailUrl
//        ];
//    }
}
