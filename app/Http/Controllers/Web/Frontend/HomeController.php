<?php

namespace App\Http\Controllers\Web\Frontend;

use App\Models\LoanType;
use App\Models\PropertyType;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home.index',[
            'propertyTypes' => PropertyType::active()->get(),
            'loanTypes' => LoanType::active()->get()
        ]);
    }
}
