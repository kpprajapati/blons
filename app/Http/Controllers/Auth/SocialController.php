<?php

namespace App\Http\Controllers\Auth;

use App\Models\Agent;
use App\Models\Customer;
use App\Models\Image;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Validator;

class SocialController extends Controller
{
    public function getSocialRedirect($provider)
    {
        $providerKey = Config::get('services.' . $provider);

        if (empty($providerKey)) {

            return view('pages.status')
                ->with('error', 'No such provider');
        }

        return Socialite::driver($provider)->redirect();

    }

    public function getSocialHandle($provider)
    {
        try {
            if (Input::get('denied') != '') {
                return redirect(route('frontend.login.index'))->with(['login_response_error' => 'Something went wrong ! please try again']);
            }

            $socialUser = Socialite::driver($provider)->user();

            //        $gmailAvatar = $socialUser->avatar_original;
            //        $gmailName = $socialUser->name;
            //        $gmailToken = $socialUser->token;
            //        $gmailId = $socialUser->id;
            //        $gmailFirstName = $socialUser->user['name']['givenName'];
            //        $gmailLastName = $socialUser->user['name']['familyName'];

            $userModel = new User();
            if (!$user = $userModel->where('email', $socialUser->email)->first()) {
                $user = $this->signUp($socialUser,$provider);
                \Auth::login($user);
                return redirect(route('frontend.home.index'));
            }

            if($user->image)
            {
                $user->image->original_url = $socialUser->avatar_original;
                $user->image->medium_url = $socialUser->avatar_original;
                $user->image->thumb_url = $socialUser->avatar_original;
                $user->image->save();
            }

            \Auth::login($user);

            return redirect(route('frontend.home.index'));
        }
        catch (\Exception $e)
        {
            return redirect(route('frontend.login.index'))->with(['login_response_error' => 'Something went wrong ! please try again']);
        }

    }


    public function signUp($socialUser,$provider)
    {
        $email = $socialUser->email;

        if ($user = User::where('email', $email)->first()) {
            return redirect(route('frontend.login.index'))->with(['login_response_error' => 'Email Already exist !']);
        }

        $userModel = new User();
        $user = $userModel->create([
            'name' => $provider == 'google' ? $socialUser->user['name']['givenName'] . ' ' . $socialUser->user['name']['familyName'] : $socialUser->name ,
            'first_name' => $provider == 'google' ? $socialUser->user['name']['givenName'] : $socialUser->name,
            'last_name' => $provider == 'google' ? $socialUser->user['name']['familyName'] : '',
            'email' => $email,
            'password' => null,
            'status' => User::ACTIVE
        ]);

        $imageModel = new Image();
        $imageModel->create([
           'original_url' => $socialUser->avatar_original,
           'medium_url' => $socialUser->avatar_original,
           'thumb_url' => $socialUser->avatar_original,
            'user_id' => $user->id
        ]);
//                $user->attachRole(Role::name('customer')->first());
//
//                $customerModel = new Customer();
//                $customerModel->create([
//                    'user_id' => $user->id
//                ]);
        return $user;
    }


    public function selectType(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|in:1,2',
        ]);

        if ($validator->fails()) {
            return redirect(route('frontend.home.index'));
        }

        $user = Auth::user();

        if ($request->get('type') == 2) {

            $user->attachRole(Role::name('customer')->first());

            $customerModel = new Customer();
            $customerModel->create([
                'user_id' => $user->id
            ]);
        }

        if ($request->get('type') == 1) {

            $user->attachRole(Role::name('agent')->first());

            $agentModel = new Agent();
            $agentModel->create([
                'user_id' => $user->id
            ]);
        }

        return redirect(route('frontend.home.index'));

    }
}
