<?php

namespace App\Listeners;

use App\Events\LoanRequestEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LoanRequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoanRequestEvent  $event
     * @return void
     */
    public function handle(LoanRequestEvent $event)
    {
        //
    }
}
