<?php


Route::group(['namespace' => 'Web\Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
    // Authentication Routes...
    Route::get('/', 'LoginController@index')->name('login.index');
    Route::get('login', 'LoginController@index')->name('login.index');
    Route::post('login/create', 'LoginController@create')->name('login.create');
    Route::get('logout', 'LoginController@logout')->name('logout');

    Route::group(['middleware' => 'AdminAuth'], function () {

        Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');

        Route::get('propertyType', 'PropertyTypeController@index')->name('propertyType.index');
        Route::get('propertyType/add', 'PropertyTypeController@add')->name('propertyType.add');
        Route::post('propertyType/create', 'PropertyTypeController@create')->name('propertyType.create');
        Route::get('propertyType/edit/{id}', 'PropertyTypeController@edit')->name('propertyType.edit');
        Route::post('propertyType/update/{id}', 'PropertyTypeController@update')->name('propertyType.update');

        Route::get('loanType', 'LoanTypeController@index')->name('loanType.index');
        Route::get('loanType/add', 'LoanTypeController@add')->name('loanType.add');
        Route::post('loanType/create', 'LoanTypeController@create')->name('loanType.create');
        Route::get('loanType/edit/{id}', 'LoanTypeController@edit')->name('loanType.edit');
        Route::post('loanType/update/{id}', 'LoanTypeController@update')->name('loanType.update');

        Route::get('question', 'QuestionController@index')->name('question.index');
        Route::get('question/add', 'QuestionController@add')->name('question.add');
        Route::post('question/create', 'QuestionController@create')->name('question.create');
        Route::get('question/edit/{id}', 'QuestionController@edit')->name('question.edit');
        Route::post('question/update/{id}', 'QuestionController@update')->name('question.update');

        Route::get('inquiry', 'InquiryController@index')->name('inquiry.index');
        Route::get('inquiry/detail/{id}', 'InquiryController@detail')->name('inquiry.detail');
        Route::get('inquiry/getAttachment', 'InquiryController@getAttachment')->name('inquiry.getAttachment');

        Route::get('user', 'UserController@index')->name('user.index');
        Route::get('user/detail/{id}', 'UserController@detail')->name('user.detail');

        Route::group(['namespace' => 'Api', 'prefix' => 'adminApi' , 'as' => 'api.'], function (){
        });

    });

});
