<?php

$s = 'social.';
Route::get('/social/redirect/{provider}',   ['as' => $s . 'redirect',   'uses' => 'Auth\SocialController@getSocialRedirect']);
Route::get('/social/handle/{provider}',     ['as' => $s . 'handle',     'uses' => 'Auth\SocialController@getSocialHandle']);
Route::post('/social/selectType',     ['as' => $s . 'selectType',     'uses' => 'Auth\SocialController@selectType']);


Route::group(['namespace' => 'Web\Frontend', 'as' => 'frontend.'], function () {
    Route::get('/','HomeController@index')->name('home.index');

    Route::get('register', 'RegisterController@index')->name('register.index');
    Route::post('register/signUp', 'RegisterController@signUp')->name('register.signUp');
    Route::get('login', 'LoginController@index')->name('login.index');
    Route::post('login/signIn', 'LoginController@signIn')->name('login.signIn');
    Route::get('logout', 'LoginController@logout')->name('login.logout');

    Route::group(['middleware' => 'CustomerAuth'], function () {
        Route::get('loan', 'LoanController@index')->name('loan.index');
        Route::get('loan/detail/{id}', 'LoanController@detail')->name('loan.detail');
        Route::get('loan/requestList', 'LoanController@requestList')->name('loan.requestList');
    });

    Route::group(['namespace' => 'Api', 'prefix' => 'frontendApi' , 'as' => 'api.'], function (){
        Route::post('loan/sendLoanRequest', 'LoanController@sendLoanRequest')->name('loan.sendLoanRequest');
        Route::post('image/attachment', 'ImageController@attachDoc')->name('image.attachDoc');
        Route::get('getQuestionsFormLoanType', 'QuestionController@getQuestionsFormLoanType')->name('question.getQuestionsFormLoanType');
    });
});


