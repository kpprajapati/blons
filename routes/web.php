<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});
//
//Route::group(['namespace' => 'Web\Admin\Api', 'as' => 'admin.api.'], function () {
//    Route::post('images/uploadPropertyImage', 'ImageController@uploadPropertyImage')->name('image.uploadPropertyImage');
//});


use App\Events\eventTrigger;
use App\Events\SendDocVerificationEvent;

Route::get('/alertBox', function () {
    return view('eventListener');
});

Route::get('/fireEvent', function () {
    event(new eventTrigger());
});

Route::get('/doc', function () {
    event(new SendDocVerificationEvent());
});

Route::get('/not', function () {
    return view('noti');
});