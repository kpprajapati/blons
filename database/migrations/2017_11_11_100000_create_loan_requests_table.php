<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('request_number')->nullable();
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('bo_name')->nullable();
            $table->string('bo_email')->nullable();
            $table->string('bo_phone')->nullable();
            $table->string('bo_address')->nullable();
            $table->string('bo_postal_code')->nullable();
            $table->date('bo_dob')->nullable();
            $table->string('credit_score')->nullable();
            $table->text('address')->nullable();
            $table->string('current_value')->nullable();
            $table->string('expected_value')->nullable();
            $table->string('year_built')->nullable();
            $table->string('sq_fit')->nullable();
            $table->string('number_limits')->nullable();
            $table->integer('property_type_id')->unsigned()->nullable();
            $table->integer('loan_type_id')->unsigned()->nullable();
            $table->string('purchase_price')->nullable();
            $table->string('amount_of_loan_request')->nullable();
            $table->string('loan_terms')->nullable();
            $table->text('ask')->nullable();
            $table->string('dp_percentage')->nullable();
            $table->string('net_amount')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0:pending,1:accept');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('property_type_id')->references('id')->on('property_types');
            $table->foreign('loan_type_id')->references('id')->on('loan_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_requests');
    }
}
