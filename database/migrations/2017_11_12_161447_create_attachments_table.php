<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('loan_request_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->text('original_url')->nullable();
            $table->text('medium_url')->nullable();
            $table->text('thumb_url')->nullable();
            $table->tinyInteger('doc_type')->default(1)->comment('1:Image,2:Other');
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
