<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('question')->nullable();
            $table->integer('loan_type_id')->unsigned()->nullable();
            $table->tinyInteger('input_type')->comment('1:Input box,2:Text area')->default(0);
            $table->tinyInteger('status')->comment('0:in-active,1:active')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('loan_type_id')->references('id')->on('loan_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
