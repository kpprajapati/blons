<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'kiran',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123'),
            'status' => 1,
        ]);

        Role::create([
           'name' => 'admin',
           'display_name' => 'Admin',
           'description' => 'Admin user'
        ]);

        $role = Role::where('name', 'admin')->first();

        $user->attachRole($role->id);

    }
}
