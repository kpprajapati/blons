@extends('frontend.main')
@section('title')
    Home
@endsection

@section('css')
@endsection

@section('content')
    <div class="slider" id="slider">
        <!-- slider -->
        <div class="slider-img">
            <img src="/frontend/images/slider-1.jpg" alt="Borrow - Loan Company Website Template" class="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="slider-captions">
                            <!-- slider-captions -->
                            <h1 class="slider-title">Hotel & Motel Loans up to $25 Millions </h1>
                            <p class="slider-text hidden-xs">Send us request today
                                <br>
                                <strong class="text-highlight">Finance up to 85 to 90 % of the Purchase price</strong></p>
                            <a href="#" class="btn btn-default hidden-xs">Send Request</a> </div>
                        <!-- /.slider-captions -->
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="slider-img"><img src="/frontend/images/slider-2.jpg" alt="Borrow - Loan Company Website Template" class="">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="slider-captions">
                                <!-- slider-captions -->
                                <h1 class="slider-title"> Commercial Loans </h1>
                                <p class="slider-text hidden-xs"> Send us request today</p>
                                <a href="#" class="btn btn-default hidden-xs">Send Request</a> </div>
                            <!-- /.slider-captions -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="slider-img"><img src="/frontend/images/slider-3.jpg" alt="Borrow - Loan Company Website Template" class="">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="slider-captions">
                                <!-- slider-captions -->
                                <h1 class="slider-title">Residential Loans</h1>
                                <p class="slider-text hidden-xs">Send us request today</p>
                                <a href="#" class="btn btn-default hidden-xs">Send Request</a> </div>
                            <!-- /.slider-captions -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="slider-img"><img src="/frontend/images/slider-3.jpg" alt="Borrow - Loan Company Website Template" class="">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="slider-captions">
                                <!-- slider-captions -->
                                <h1 class="slider-title">SBA Loans</h1>
                                <p class="slider-text hidden-xs">Send us request today</p>
                                <a href="#" class="btn btn-default hidden-xs">Send Request</a> </div>
                            <!-- /.slider-captions -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{--<div class="rate-table">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--@foreach($loanTypes as $index => $type)--}}
                {{--<div class="col-md-3 col-sm-6 col-xs-6">--}}
                    {{--<div class="rate-counter-block">--}}
                        {{--<div class="icon rate-icon  "> <img src="/frontend/images/mortgage.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-1x"></div>--}}
                        {{--<div class="rate-box">--}}
                            {{--<h1 class="loan-rate">{{ $type->loan_rate }}%</h1>--}}
                            {{--<small class="rate-title">{{ $type->name }}</small>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--@endforeach--}}
                {{--<div class="col-md-3 col-sm-6 col-xs-6">--}}
                    {{--<div class="rate-counter-block">--}}
                        {{--<div class="icon rate-icon  "> <img src="/frontend/images/loan.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-1x"></div>--}}
                        {{--<div class="rate-box">--}}
                            {{--<h1 class="loan-rate">8.96%</h1>--}}
                            {{--<small class="rate-title">Personal Loans</small>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-6 col-xs-6">--}}
                    {{--<div class="rate-counter-block">--}}
                        {{--<div class="icon rate-icon  "> <img src="/frontend/images/car.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-1x"></div>--}}
                        {{--<div class="rate-box">--}}
                            {{--<h1 class="loan-rate">6.70%</h1>--}}
                            {{--<small class="rate-title">Car Loans</small>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-6 col-xs-6">--}}
                    {{--<div class="rate-counter-block">--}}
                        {{--<div class="icon rate-icon  "> <img src="/frontend/images/credit-card.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-1x"></div>--}}
                        {{--<div class="rate-box">--}}
                            {{--<h1 class="loan-rate">9.00%</h1>--}}
                            {{--<small class="rate-title">Credit card</small>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="section-space80">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
                    <div class="mb60 text-center section-title">
                        <!-- section title start-->
                        <h1>Property Types</h1>
                        {{--<p>We will match you with a loan program that meet your financial need. In short term liquidity, by striving to make funds available to them <strong>within 24 hours of application.</strong></p>--}}
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="service">

                    @foreach($propertyTypes as $index => $type)
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="bg-white pinside40 service-block outline mb30">
                            <div class="icon mb40"> <img src="{{ $type->image->original_url }}" alt="Borrow - Loan Company Website Template" class=""></div>
                            <h2><a href="#" class="title"> {{ $type->name }}</a></h2>
                            <p class="text-justify">{{ $type->description }} </p>
                            <a href="{{ route('frontend.loan.index') }}" class="btn-link">Send Request</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white section-space80">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 col-xs-12">
                    <div class="mb100 text-center section-title">
                        <!-- section title start-->
                        <h1>Funding Needs</h1>
                        {{--<p>Suspendisse aliquet varius nunc atcibus lacus sit amet coed portaeri sque mami luctus viveed congue lobortis faucibus.</p>--}}
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">

                @foreach($loanTypes as $index => $type)
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="bg-white pinside40 number-block outline mb60 bg-boxshadow">
                        <div class="circle"><span class="number">{{ $index + 1 }}</span></div>
                        <h3 class="number-title">{{ $type->name }}</h3>
                        <p>{{ $type->description }}</p>
                    </div>
                </div>
                @endforeach

            </div>
            {{--<div class="row">--}}
                {{--<div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 text-center"> <a href="#" class="btn btn-default">View Our Loans</a> </div>--}}
            {{--</div>--}}
        </div>
    </div>
    {{--<div class="section-space80">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-offset-2 col-md-8">--}}
                    {{--<div class="mb60 text-center section-title">--}}
                        {{--<!-- section title start-->--}}
                        {{--<h1>Why People Choose Us</h1>--}}
                        {{--<p>Suspendisse aliquet varius nunc atcibus lacus sit amet coed portaeri sque mami luctus viveed congue lobortis faucibus.</p>--}}
                    {{--</div>--}}
                    {{--<!-- /.section title start-->--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
                    {{--<div class="bg-white bg-boxshadow">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-4 col-sm-6 nopadding col-xs-12">--}}
                                {{--<div class="bg-white pinside60 number-block outline">--}}
                                    {{--<div class="mb20"><i class="icon-command  icon-4x icon-primary"></i></div>--}}
                                    {{--<h3>Dedicated Specialists</h3>--}}
                                    {{--<p>Duis eget diam quis elit erdiet alidvolutp terdum tfanissim non intwesollis eu mauris.</p>--}}
                                    {{--<a href="#" class="btn btn-outline mt20">Meet the team</a> </div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4 col-sm-6 nopadding col-xs-12">--}}
                                {{--<div class="bg-white pinside60 number-block outline">--}}
                                    {{--<div class="mb20"><i class="icon-cup  icon-4x icon-primary"></i></div>--}}
                                    {{--<h3>Success Stories Rating</h3>--}}
                                    {{--<p>Integer facilisis fringilla dolor ut luctus lvinar felis miat velitliquam at fermentum orci.</p>--}}
                                    {{--<a href="#" class="btn btn-outline mt20">View Client Review</a> </div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4 col-sm-12 nopadding col-xs-12">--}}
                                {{--<div class="bg-white pinside60 number-block outline">--}}
                                    {{--<div class="mb20"><i class="icon-calculator  icon-4x icon-primary"></i></div>--}}
                                    {{--<h3>No front Appraisal Fees!</h3>--}}
                                    {{--<p> Integer faisis fringilla dolor ut luctus nisi eneinar felis viverra dignissim fermentum orci.</p>--}}
                                    {{--<a href="#" class="btn btn-outline mt20">Why choose us</a> </div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="bg-default section-space80">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">--}}
                    {{--<div class="mb60 text-center section-title">--}}
                        {{--<!-- section title start-->--}}
                        {{--<h1 class="title-white">Some of our Awesome Testimonials</h1>--}}
                        {{--<p> You won’t be the only one lorem ipsu mauris diam mattises.</p>--}}
                    {{--</div>--}}
                    {{--<!-- /.section title start-->--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-4 col-sm-4 clearfix col-xs-12">--}}
                    {{--<div class="testimonial-block mb30">--}}
                        {{--<div class="bg-white pinside30 mb20">--}}
                            {{--<p class="testimonial-text"> “I loved the customer service you guys provided me. That was very nice and patient with questions I had. I would really like definitely come back here”</p>--}}
                        {{--</div>--}}
                        {{--<div class="testimonial-autor-box">--}}
                            {{--<div class="testimonial-img pull-left"> <img src="/frontend/images/testimonial-img.jpg" alt="Borrow - Loan Company Website Template"> </div>--}}
                            {{--<div class="testimonial-autor pull-left">--}}
                                {{--<h4 class="testimonial-name">Donny J. Griffin</h4>--}}
                                {{--<span class="testimonial-meta">Personal Loan</span> </div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 clearfix col-xs-12">--}}
                    {{--<div class="testimonial-block mb30">--}}
                        {{--<div class="bg-white pinside30 mb20">--}}
                            {{--<p class="testimonial-text"> “I had a good experience with Insight Loan Services. I am thankful to insight for the help you guys gave me. My loan was easy and fast. thank you Insigtht”</p>--}}
                        {{--</div>--}}
                        {{--<div class="testimonial-autor-box">--}}
                            {{--<div class="testimonial-img pull-left"> <img src="/frontend/images/testimonial-img-1.jpg" alt="Borrow - Loan Company Website Template"> </div>--}}
                            {{--<div class="testimonial-autor pull-left">--}}
                                {{--<h4 class="testimonial-name">Mary O. Randle</h4>--}}
                                {{--<span class="testimonial-meta">Education Loan</span> </div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-4 clearfix col-xs-12">--}}
                    {{--<div class="testimonial-block mb30">--}}
                        {{--<div class="bg-white pinside30 mb20">--}}
                            {{--<p class="testimonial-text"> “We came out of their offices very happy with their service. They treated us very kind. Definite will come back. The waiting time was very appropriate.”</p>--}}
                        {{--</div>--}}
                        {{--<div class="testimonial-autor-box">--}}
                            {{--<div class="testimonial-img pull-left"> <img src="/frontend/images/testimonial-img-2.jpg" alt="Borrow - Loan Company Website Template"> </div>--}}
                            {{--<div class="testimonial-autor pull-left">--}}
                                {{--<h4 class="testimonial-name">Lindo E. Olson</h4>--}}
                                {{--<span class="testimonial-meta">Car Loan</span> </div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="section-space40 bg-white">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-2 col-sm-4 col-xs-6"> <img src="/frontend/images/logo-1.jpg" alt="Borrow - Loan Company Website Template"> </div>--}}
                {{--<div class="col-md-2 col-sm-4 col-xs-6"> <img src="/frontend/images/logo-2.jpg" alt="Borrow - Loan Company Website Template"> </div>--}}
                {{--<div class="col-md-2 col-sm-4 col-xs-6"> <img src="/frontend/images/logo-3.jpg" alt="Borrow - Loan Company Website Template"> </div>--}}
                {{--<div class="col-md-2 col-sm-4 col-xs-6"> <img src="/frontend/images/logo-4.jpg" alt="Borrow - Loan Company Website Template"> </div>--}}
                {{--<div class="col-md-2 col-sm-4 col-xs-6"> <img src="/frontend/images/logo-5.jpg" alt="Borrow - Loan Company Website Template"> </div>--}}
                {{--<div class="col-md-2 col-sm-4 col-xs-6"> <img src="/frontend/images/logo-1.jpg" alt="Borrow - Loan Company Website Template"> </div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="section-space80">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">--}}
                    {{--<div class="mb60 text-center section-title">--}}
                        {{--<!-- section title start-->--}}
                        {{--<h1>Latest News from Loan Company</h1>--}}
                        {{--<p> Our mission is to deliver reliable, latest news and opinions.</p>--}}
                    {{--</div>--}}
                    {{--<!-- /.section title start-->--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-4 col-sm-6 col-xs-12">--}}
                    {{--<div class="post-block mb30">--}}
                        {{--<div class="post-img">--}}
                            {{--<a href="blog-single.html" class="imghover"><img src="/frontend/images/blog-img.jpg" alt="Borrow - Loan Company Website Template" class="img-responsive"></a>--}}
                        {{--</div>--}}
                        {{--<div class="bg-white pinside40 outline">--}}
                            {{--<h2><a href="blog-single.html" class="title">Couples Buying New Home Loan</a></h2>--}}
                            {{--<p class="meta"><span class="meta-date">Aug 25, 2017</span><span class="meta-author">By<a href="#"> Admin</a></span></p>--}}
                            {{--<p>Fusce sed erat libasellus id orci quis ligula pret do lectus velit, a malesuada urna sodales eu.</p>--}}
                            {{--<a href="blog-single.html" class="btn-link">Read More</a> </div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-6 col-xs-12">--}}
                    {{--<div class="post-block mb30">--}}
                        {{--<div class="post-img">--}}
                            {{--<a href="blog-single.html" class="imghover"><img src="/frontend/images/blog-img-1.jpg" alt="Borrow - Loan Company Website Template" class="img-responsive"></a>--}}
                        {{--</div>--}}
                        {{--<div class="bg-white pinside40 outline">--}}
                            {{--<h2><a href="blog-single.html" class="title">Business Man Thinking for Loan</a></h2>--}}
                            {{--<p class="meta"><span class="meta-date">Aug 24, 2017</span><span class="meta-author">By<a href="#"> Admin</a></span></p>--}}
                            {{--<p>Nulla vehicula nibh vel malesuada dapibus ringilla nunc mi sit amet fbendum sapierttitor nibh. </p>--}}
                            {{--<a href="blog-single.html" class="btn-link">Read More</a> </div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-6 col-xs-12">--}}
                    {{--<div class="post-block mb30">--}}
                        {{--<div class="post-img">--}}
                            {{--<a href="blog-single.html" class="imghover"><img src="/frontend/images/blog-img-2.jpg" alt="Borrow - Loan Company Website Templates" class="img-responsive"></a>--}}
                        {{--</div>--}}
                        {{--<div class="bg-white pinside40 outline">--}}
                            {{--<h2><a href="blog-single.html" class="title">Are you students looking for loan ?</a></h2>--}}
                            {{--<p class="meta"><span class="meta-date">Aug 23, 2017</span><span class="meta-author">By<a href="#"> Admin</a></span></p>--}}
                            {{--<p>Malesuada urna sodales euusce sed erat libasellus id orci quis ligula pretium co ctus velit.</p>--}}
                            {{--<a href="blog-single.html" class="btn-link">Read More</a> </div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="section-space80 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">
                    <div class="mb60 text-center section-title">
                        <!-- section title-->
                        <h1>We are Here to Help You</h1>
                        <p>Our mission is to deliver reliable, latest news and opinions.</p>
                    </div>
                    <!-- /.section title-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="bg-white bg-boxshadow pinside40 outline text-center mb30" style="padding-bottom: 17%;">
                        <div class="mb40">
                            <i class="icon-calendar-3 icon-2x icon-default"></i>
                        </div>
                        <h2 class="capital-title">Apply For Loan</h2>
                        <p>Looking to buy a property or refinance.  start hear.</p>
                        <a href="#" class="btn-link">Apply Now!</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="bg-white bg-boxshadow pinside40 outline text-center mb30">
                        <div class="mb40"><i class="icon-phone-call icon-2x icon-default"></i></div>
                        <h2 class="capital-title">Call us at </h2>
                        <h1 class="text-big">800-123-456 / 789 </h1>
                        <p>support@basrow.com</p>
                        <a href="mailto:support@basrow.com" class="btn-link">Contact us</a> </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="bg-white bg-boxshadow pinside40 outline text-center mb30" style="padding-bottom: 38%;">
                        <div class="mb40"> <i class="icon-users icon-2x icon-default"></i></div>
                        <h2 class="capital-title">SBA Loans</h2>
                        <p>Best suited for small business.</p>
                        {{--<a href="#" class="btn-link">Meet The Advisor</a> </div>--}}
                </div>
            </div>

        </div>
    </div>
@endsection

@section('js')
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/frontend/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/frontend/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/frontend/js/menumaker.js"></script>
    <!-- animsition -->
    <script type="text/javascript" src="/frontend/js/animsition.js"></script>
    <script type="text/javascript" src="/frontend/js/animsition-script.js"></script>
    <!-- sticky header -->
    <script type="text/javascript" src="/frontend/js/jquery.sticky.js"></script>
    <script type="text/javascript" src="/frontend/js/sticky-header.js"></script>
    <!-- slider script -->
    <script type="text/javascript" src="/frontend/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="/frontend/js/slider-carousel.js"></script>
    <script type="text/javascript" src="/frontend/js/service-carousel.js"></script>
    <!-- Back to top script -->
    <script src="/frontend/js/back-to-top.js" type="text/javascript"></script>
@endsection