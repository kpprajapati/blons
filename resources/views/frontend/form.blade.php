<style>
    .error{
        color:red;
    }
</style>
<div class="section-scroll" id="section-apply">
    <div class="bg-light pinside60">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-title mb60 text-center">
                    <h1>Get a Quote</h1>
                    <p>Now apply for a Loan online, All you need to do is provide your details below application form.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form id="loanForm" class="">
                    <fieldset>
                        <!-- Text input-->
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label class="control-label" for="name">Name</label>
                            <div class="">
                                <input id="name" name="name" type="text" placeholder="Name" class="form-control input-md">
                                <span class="help-block"> </span> </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label class=" control-label" for="email">E-Mail</label>
                            <div class="">
                                <input id="email" name="email" type="text" placeholder="E-mail" class="form-control input-md">
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group col-md-4 col-sm-12 col-xs-12">
                            <label class="control-label" for="phone">Phone</label>
                            <div class="">
                                <input id="phone" name="phone" type="text" placeholder="Phone" class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-sm-12 slide-ranger col-xs-12">
                            <p id="slider-range-min"></p>
                            <label for="amount" class="control-label">Loan Amount</label>
                            <input type="text" id="amount" readonly class="form-control">
                        </div>
                        <div class="form-group col-md-6 col-sm-12 slide-ranger col-xs-12">
                            <p id="slider-range-max"></p>
                            <label for="amount" class="control-label">Year</label>
                            <input type="text" id="j" readonly class="form-control">
                        </div>


                        <div class="form-group col-md-12">
                            <label class="control-label">Attachment</label>
                            <input type="file" required="required" class="form-control prop_image" />
                        </div>

                        <div class="col-md-12 images"></div>

                        <div class="form-group col-md-12">
                            <label for="description" class="control-label">Description</label>
                            <textarea id="desc" name="ask" title="description" class="form-control" rows="5"></textarea>
                        </div>

                        <!-- Button -->
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <div class="">
                                <button id="send" name="Submit" class="btn btn-primary btn-block">Submit New Quote</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

@section('js')
    <script src="/frontend/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/frontend/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/frontend/js/menumaker.js"></script>
    <!-- animsition -->
    <script type="text/javascript" src="/frontend/js/animsition.js"></script>
    <script type="text/javascript" src="/frontend/js/animsition-script.js"></script>
    <!-- sticky header -->
    <script type="text/javascript" src="/frontend/js/jquery.sticky.js"></script>
    <script type="text/javascript" src="/frontend/js/sticky-header.js"></script>
    <!-- one page scroling navigation -->
    <script type="text/javascript" src="/frontend/js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="/frontend/js/scrolling-nav.js"></script>
    <!-- Faq Accordion -->
    <script src="/frontend/js/accordion.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript" src="/frontend/js/validation/jquery.validate.js"></script>

    <script>
        $(function() {
            $("#slider-range-min").slider({
                range: "min",
                value: 3000,
                min: 1000,
                max: 500000,
                slide: function(event, ui) {
                    $("#amount").val("$" + ui.value);
                }
            });
            $("#amount").val("$" + $("#slider-range-min").slider("value"));
        });

        $(function() {
            $("#slider-range-max").slider({
                range: "max",
                min: 1,
                max: 10,
                value: 2,
                slide: function(event, ui) {
                    $("#j").val(ui.value);
                }
            });
            $("#j").val($("#slider-range-max").slider("value"));
        });

        $("#loanForm").validate({
            rules: {
                name : {
                    required : true,
                },
                email : {
                    required : true,
                    email : true
                },
                phone : {
                    required : true,
                    number : true
                }
            },
            messages: {
                name : {
                    required : "Field is required",
                },
                email : {
                    required : "Field is required",
                    email : "Please enter valid email"
                },
                phone : {
                    required : "Field is required",
                    number : "Please enter valid phone"
                }
            }
        });


        var clicks = 0;
        $("body").on("change",".prop_image",function() {

            var ext = this.value.match(/\.(.+)$/)[1];

            if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'pdf' || ext == 'PDF')
            {
                showLoading();
                var html = '';
                var file_data = $(this).prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('_token', '{{ csrf_token() }}');
                $.ajax({
                    url: '{{ route('frontend.api.image.attachDoc') }}', // point to server-side PHP script
                    dataType: 'json',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(data){
                        hideLoading();
                        if(data.status == true)
                        {
                            var uurl = ''; var types = 1;
                            if(ext == 'pdf' || ext == 'PDF')
                            {
                                types = 2;
                                uurl = '/frontend/pdf.png';
                            }
                            else
                            {
                                uurl = data.medium_url;
                            }
                            html +='<div class="col-md-3 prop-img prop-images'+clicks+'" data-file-name="'+data.image_name+'" data-file-original="'+data.original_url+'" data-file-medium="'+data.medium_url+'" data-file-thumb="'+data.thumb_url+'" data-types="'+types+'" style="margin-bottom: 3%;">';
                            html +='<img style="margin-left:0%;box-shadow:0px 0px 2px gray;padding:5px;max-height:150px;max-width: 150px;" src="'+uurl+'" height="150" width="150"/>';
                            html +='<span style="margin-left:2%;margin-right:2%;margin-top:-40%;cursor: pointer;background-color: #E36758;" data-class="prop-images'+clicks+'" data-file-name="'+data.image_name+'" data-file-original="'+data.original_url+'" data-file-medium="'+data.medium_url+'" data-file-thumb="'+data.thumb_url+'" class="remove_image label bg-danger" title="Remove"><i class="fa fa-trash-o"></i></span>';
                            html +='</div>';
                            $(".images").append(html);
                            clicks++;
                        }
                        else
                        {
                            snackBarError('Something went wrong. please try again !');
                        }
                    },error:function(error){
                        hideLoading();
                    }
                });
                return false;
            }
            else
            {
                hideLoading();
                snackBarError('Something went wrong. please try again !');
            }
        });

        $("body").on("click",".remove_image",function() {
            var singleClass = $(this).attr('data-class');
            $("." + singleClass).remove();
        });

        $('body').on('click','#send',function () {
            if($("#loanForm").valid()) {
                var name = $('#name').val();
                var email = $('#email').val();
                var phone = $('#phone').val();
                var amount = $('#amount').val();
                var year = $('#j').val();
                var desc = $('#desc').val();

                var imageArray = [];
                $('.prop-img').each(function() {
                    var fileName = $(this).attr('data-file-name');
                    var originalUrl = $(this).attr('data-file-original');
                    var mediumUrl = $(this).attr('data-file-medium');
                    var thumbUrl = $(this).attr('data-file-thumb');
                    var types = $(this).attr('data-types');
                    imageArray.push({
                        'fileName' : fileName,
                        'originalUrl' : originalUrl,
                        'mediumUrl' : mediumUrl,
                        'thumbUrl' : thumbUrl,
                        'types' : types
                    });
                });

                $.ajax({
                    method: 'POST',
                    data: {
                        '_token': "{{ csrf_token() }}",
                        'name': name,
                        'email': email,
                        'phone': phone,
                        'loan_amount': amount,
                        'terms': year,
                        'description': desc,
                        'loan_type': 'personal loan',
                        'imageArray' : imageArray
                    },
                    url: "{{ route('frontend.api.loan.sendLoanRequest') }}",
                    type: 'json',
                    success: function () {
                        snackBarSuccess('Request Successfully sent');
                        setTimeout(function () {
                            window.location.reload();
                        },1000);
                    }, error: function (error) {
                        snackBarError('Something went wrong please try again later !');
                    }
                });
                return false;
            }
        });
    </script>
    <!-- Back to top script -->
    <script src="/frontend/js/back-to-top.js" type="text/javascript"></script>
@endsection