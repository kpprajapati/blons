@extends('frontend.main')

@section('title')
    Login
@endsection

@section('css')
    <style>
        label
        {
            font-weight: normal;
        }
    </style>
@endsection

@section('content')
    {{--<div class="page-header">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="page-breadcrumb">--}}
                        {{--<ol class="breadcrumb">--}}
                            {{--<li><a href="{{ route('frontend.home.index') }}">Home</a></li>--}}
                            {{--<li class="active">Login</li>--}}
                        {{--</ol>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
                    {{--<div class="bg-white pinside30">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-4 col-sm-5">--}}
                                {{--<h1 class="page-title">Contact us</h1>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8 col-sm-7">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-12 col-sm-12">--}}
                                        {{--<div class="btn-action"> <a href="#" class="btn btn-default">How To Apply</a> </div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="sub-nav" id="sub-nav">--}}
                        {{--<ul class="nav nav-justified">--}}
                            {{--<li><a href="contact-us.html">Give me call back</a></li>--}}
                            {{--<li><a href="#">Emi Caculator</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="col-md-12">&nbsp;</div>
    <div class="col-md-12">&nbsp;</div>
    <div class="col-md-12">&nbsp;</div>
    <div class=" ">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-3">&nbsp;</div>
                <div class="col-md-6">
                    <div class="wrapper-content bg-white pinside40">
                        <div class="contact-form mb60">
                            <div class=" ">
                                <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
                                    <div class="mb60  section-title text-center  ">
                                        <!-- section title start-->
                                        <h1>Sign in</h1>
                                        <p>Already have an account ! Sign in</p>
                                    </div>
                                </div>

                                {{--<div class="col-md-12 text-center no-padding">--}}
                                    {{--<!--<h3>Logging Or Sign up </h3><br>-->--}}
                                    {{--<a href="{{ route('social.redirect', ['provider' => 'facebook']) }}">--}}
                                        {{--<button class="btn btn-primary"  style="width: 49%; font-size: 17px;background-color: #3b5998;"><i class="fa fa-facebook-f margin-right-10" style="font-size: 17px;"></i> Facebook</button>--}}
                                    {{--</a>--}}
                                    {{--<a href="{{ route('social.redirect', ['provider' => 'google']) }}">--}}
                                        {{--<button class="btn btn-primary" style="width: 49%;font-size: 17px; background-color: #d62d20;border-color: #d62d20;"> <i class="fa fa-google margin-right-10" style="font-size: 17px; "></i> Google</button>--}}
                                    {{--</a>--}}
                                    {{--<br><br>OR<br><br>--}}
                                {{--</div>--}}

                                <div class="row">
                                    <form class="contact-us" method="post" action="{{ route('frontend.login.signIn') }}">
                                        {{ csrf_field() }}
                                        <div class=" ">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="sr-only control-label" for="email">Email<span class=" "> </span></label>
                                                        <input id="email" name="email" type="email" placeholder="Email" class="form-control input-md">
                                                        @foreach($errors->get('email') as $error)
                                                            <label class="validation-error-label text-danger" for="email">
                                                                {{ $error }}
                                                            </label>
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="sr-only control-label" for="password">password<span class=" "> </span></label>
                                                        <input id="password" name="password" type="password" placeholder="Password" class="form-control input-md">
                                                        @foreach($errors->get('password') as $error)
                                                            <label class="validation-error-label text-danger" for="password">
                                                                {{ $error }}
                                                            </label>
                                                        @endforeach
                                                    </div>
                                                </div>

                                            <!-- Button -->
                                            <div class="col-md-12 col-xs-12">
                                                <button type="submit" class="btn btn-default">Sign in</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <p>&nbsp;</p>

                                <p class="text-center">
                                    <a href="{{ route('frontend.register.index') }}">
                                        Don't have an account ! Create Account
                                    </a>
                                </p>
                            </div>
                            <!-- /.section title start-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/frontend/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/frontend/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/frontend/js/menumaker.js"></script>
    <!-- animsition -->
    <script type="text/javascript" src="/frontend/js/animsition.js"></script>
    <script type="text/javascript" src="/frontend/js/animsition-script.js"></script>
    <!-- sticky header -->
    <script type="text/javascript" src="/frontend/js/jquery.sticky.js"></script>
    <script type="text/javascript" src="/frontend/js/sticky-header.js"></script>
    <!-- Back to top script -->
    <script src="/frontend/js/back-to-top.js" type="text/javascript"></script>

    <script>
        @if(Session::has('error'))
            snackBarError('{{ Session::get('error') }}');
        @endif
    </script>
@endsection