@extends('frontend.main')

@section('title')
    Home
@endsection
@section('css')
    <style type="text/css">
        input, input[type="text"], input[type="password"], input[type="email"], input[type="number"], textarea, select {
            height: 51px;
            line-height: 51px;
            padding: 0 20px;
            outline: none;
            font-size: 15px;
            color: #808080;
            margin: 0 0 16px 0;
            max-width: 100%;
            width: 100%;
            box-sizing: border-box;
            display: block;
            background-color: #fcfcfc;
            font-weight: 500;
            border: 1px solid #e0e0e0;
            opacity: 1;
            border-radius: 3px;}
        .error
        {
            color:red;
        }
        input, input[type="text"], input[type="password"], input[type="email"], input[type="number"], textarea, select {
            height: 51px;
            line-height: 51px;
            padding: 0 20px;
            outline: none;
            font-size: 15px;
            color: #808080;
            margin: 0 0 16px 0;
            max-width: 100%;
            width: 100%;
            box-sizing: border-box;
            display: block;
            background-color: #fcfcfc;
            font-weight: 500;
            border: 1px solid #e0e0e0;
            opacity: 1;
            border-radius: 3px;}
        .my-account label input {
            margin-top: 8px;
            padding-left: 45px;
            height: 53px;
            max-width: 100%;
            width: 500px;

        }
        .style-1 .tabs-container .tab-content {
            border: none;
            border-top: 1px solid #e0e0e0;
            padding: 30px 0 0 0;
        }
        .vd>li>a{
            font-size: 18px;
            padding: 10px 20px;
            text-align: left;

            color: #66718C;
        }

        .navbar-nav > .user-menu > .vd{
            width: 200px;
            /*padding: 5px 10px;*/
        }

    </style>
@endsection

@section('content')

    <!-- Container -->
    <div class="container">

        <div class="row">
            <div class="col-md-4 col-md-offset-4">

                <!--Tab -->
                <div class="my-account style-1 margin-top-5 margin-bottom-40">

                    <ul class="tabs-nav">
                        <li class=""><a href="#tab1">Forgot Password</a></li>
                    </ul>

                    <div class="tabs-container alt">

                        <div class="tab-content" id="tab1" style="display: none;">
                            <form id="form" name="forgotPasswordForm" method="post" class="login" action="{{ route('frontend.login.forgotPasswordEmail') }}">
                                {{ csrf_field() }}

                                @if(Session::has('success'))
                                    <div class="alert alert-success fade in alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                        <strong>Success ! : </strong> {{ Session::get('success') }}
                                    </div>
                                @endif
                                @if(Session::has('warning'))
                                @endif
                                @if(Session::has('error'))
                                    <div class="alert alert-danger fade in alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                        <strong>Error!</strong> {{ Session::get('error') }}
                                    </div>
                                @endif

                                <p class="form-row form-row-wide">
                                    <label for="username">E-Mail:
                                        <i class="im im-icon-Male"></i>
                                        <input type="text" class="input-text" name="email" id="email" placeholder="Email" value="" />
                                    </label>
                                    <span id="email_error" class="validation-error-label text-danger"></span>
                                    @foreach($errors->get('email') as $error)
                                        <label class="validation-error-label text-danger" for="email">
                                            {{ $error }}
                                        </label>
                                    @endforeach
                                </p>

                                <p class="form-row">
                                    <div class="loading" style="display: none;">Loading&#8230;</div>
                                    <input id="send" type="submit" class="button border margin-top-10" name="login" value="Get password" />
                                </p>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Container / End -->
@endsection

@section('js')
    <script type="text/javascript" src="/frontend/js/validation/jquery.validate.js"></script>
    <script>
        $("#form").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                email: {
                    required : "Please enter email address",
                    email : "Please enter valid email address"
                }
            },
            errorPlacement: function(error, element) {
                //Custom position: first name
                if (element.attr("name") == "email") {
                    $("#email_error").html(error);
                }
            }
        });

        $("body").on("click","#send",function(){
            if($('#email').val() == '')
            {
                return false;
            }
            showLoading();
            $("#form").submit();
        });
    </script>
@endsection