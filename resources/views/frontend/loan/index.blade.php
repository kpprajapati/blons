@extends('frontend.main')

@section('title')
    Loan request
@endsection

@section('css')
    <style>
        .error
        {
            color: red;
        }

        .ui-widget-content
        {
            background: white !important;
        }

        .ui-autocomplete div
        {
            background: white;
        }

        .ui-autocomplete:hover
        {
            background: white !important;
        }

        .ui-autocomplete div:hover
        {
            background: white;
        }

        .ui-menu-item
        {
            border: none !important;
        }


        .btn-default
        {
            background-color: #15549A;
        }

        label
        {
            font-weight: normal;
        }

        .stepwizard-step p {
            margin-top: 0px;
            color:#666;
        }
        .stepwizard-row {
            display: table-row;
        }
        .stepwizard {
            display: table;
            width: 100%;
            position: relative;
        }
        .stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
            opacity:1 !important;
            color:#bbb;
        }
        .progress{
            top: 13px;
            bottom: 0;
            position: absolute;
            content:" ";
            width: 74%;
            left:13%;
            height: 4px;
            z-index: 0;
        }
        .stepwizard-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }
        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
    </style>
@endsection

@section('content')
    {{--<div class="page-header">--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
    {{--<div class="page-breadcrumb">--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li><a href="{{ route('frontend.home.index') }}">Home</a></li>--}}
    {{--<li class="active">Login</li>--}}
    {{--</ol>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
    {{--<div class="bg-white pinside30">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-4 col-sm-5">--}}
    {{--<h1 class="page-title">Contact us</h1>--}}
    {{--</div>--}}
    {{--<div class="col-md-8 col-sm-7">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-12 col-sm-12">--}}
    {{--<div class="btn-action"> <a href="#" class="btn btn-default">How To Apply</a> </div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="sub-nav" id="sub-nav">--}}
    {{--<ul class="nav nav-justified">--}}
    {{--<li><a href="contact-us.html">Give me call back</a></li>--}}
    {{--<li><a href="#">Emi Caculator</a></li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="col-md-12">&nbsp;</div>
    <div class="col-md-12">&nbsp;</div>
    <div class="col-md-12">&nbsp;</div>
    <div class=" ">
        <!-- content start -->
        <div class="container">
            <div class="stepwizard">
                <div class="progress center-block">
                    <div class="progress-bar progress-bar-success active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 13%">
                    </div>
                </div>

                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step col-xs-3">
                        <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
                        <p><small>Loan details</small></p>
                    </div>
                    <div class="stepwizard-step col-xs-3">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p><small>Loan Information</small></p>
                    </div>
                    <div class="stepwizard-step col-xs-3">
                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p><small>Documents</small></p>
                    </div>
                    <div class="stepwizard-step col-xs-3">
                        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                        <p><small>Borrower</small></p>
                    </div>
                </div>
            </div>

            <form id="loanForm">
                <div class="panel panel-primary setup-content" id="step-1" style="margin:5% 0%;border:none;">
                    <div class="panel-heading" style="background-color: #15549A;">
                        <h3 class="panel-title">Loan details</h3>
                    </div>
                    <div class="panel-body">

                        <div class="form-group col-md-3">
                            <label for="sel1">Property Type <span class="error">*</span></label>
                            <select title="category" name="property_type" class="form-control" id="property_type">
                                <option value="">-- Select property type --</option>
                                @foreach($propertyTypes as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group col-md-3">
                            <label for="sel1">Loan Type <span class="error">*</span></label>
                            <select title="category" name="loan_type" class="form-control" id="loan_type">
                                <option value="">-- Select loan type --</option>
                                @foreach($loanTypes as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>


                        {{--<div class="form-group col-md-3">--}}
                            {{--<label for="sel1">Sub category <span class="error">*</span></label>--}}
                            {{--<select title="subCategory" name="subCategory" class="form-control" id="subCategory">--}}
                                {{--<option value="">-- Select subcategory --</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        {{--<div class="form-group col-md-3">--}}
                            {{--<label for="sel1">Loan type <span class="error">*</span></label>--}}
                            {{--<select title="loanType" name="loanType" class="form-control" id="loanType">--}}
                                {{--<option value="">-- Select Loan type--</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        {{--<div class="form-group col-md-3">--}}
                            {{--<label for="sel1">Sub Loan type <span class="error">*</span></label>--}}
                            {{--<select title="subLoanType" name="subLoanType" class="form-control" id="subLoanType">--}}
                                {{--<option value="">-- Select Sub loan type--</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        {{--<div class="form-group col-md-8">--}}
                            {{--<label for="sel1">Reason <span class="error">*</span></label>--}}
                            {{--<select title="reason" name="reason" class="form-control" id="reason">--}}
                                {{--<option value="">-- Select reason --</option>--}}
                                {{--@foreach($reasons as $reason)--}}
                                    {{--<option value="{{ $reason->id }}">{{ $reason->name }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        <div class="form-group col-md-4">
                            <label for="sel1">Loan terms <span class="error">*</span></label>
                            <select title="loan_term" name="loan_term" class="form-control" id="loan_term">
                                <option value="">-- select --</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="25">25</option>
                                <option value="30">30</option>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label class="sel1">Purchase price (Optional)</label>
                            <input id="purchase_price" name="purchase_price" type="text" class="form-control" placeholder="Purchase price" onkeypress="javascript:return digitOnly(event)" />
                        </div>

                        <div class="form-group col-md-3">
                            <label class="sel1">Amount of loan request <span class="error">*</span></label>
                            <input id="amount_of_loan_request" name="amount_of_loan_request" type="text" class="form-control" placeholder="Amount of loan request" onkeypress="javascript:return digitOnly(event)"/>
                        </div>

                        <div class="form-group col-md-2">
                            <label class="sel1">Down Payment (%) <span class="error">*</span></label>
                            <select title="downpayment_percentage" name="downpayment_percentage" class="form-control" id="downpayment_percentage">
                                <option value="">-- Select %--</option>
                                @for($i=1;$i<50;$i++)
                                    <option value="{{ $i }}">{{ $i }}%</option>
                                @endfor
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="sel1">Net amount <span class="error">*</span></label>
                            <input id="net_amount" name="net_amount" type="text" class="form-control" placeholder="Net amount" onkeypress="javascript:return digitOnly(event)"/>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="comment">Ask</label>
                            <textarea id="ask" name="ask" title="ask" class="form-control" rows="5"></textarea>
                        </div>

                        <!--<button class="btn btn-primary nextBtn pull-right" type="button">Next <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></button>-->

                        <nav>
                            <ul class="pager">
                                <li class="next"><a class="nextBtn" href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                            </ul>
                        </nav>

                    </div>
                </div>

                <div class="panel panel-primary setup-content" id="step-2" style="margin:5% 0%;border:none;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Loan Information</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group col-md-12">
                            <label class="control-label">Address</label>
                            <input id="address" name="address" type="text" class="form-control" placeholder="Address" />
                        </div>

                        <div class="form-group col-md-4">
                            <label class="control-label">Current value</label>
                            <input id="current_value" name="current_value" type="text" class="form-control" placeholder="Current value" onkeypress="javascript:return digitOnly(event)" />
                        </div>

                        <div class="form-group col-md-4">
                            <label class="control-label">Expected value</label>
                            <input id="expected_value" type="text" class="form-control" placeholder="Expected value" onkeypress="javascript:return digitOnly(event)" />
                        </div>

                        <div class="form-group col-md-4">
                            <label class="control-label">Year built</label>
                            <input id="year_built" name="year_built" type="text" class="form-control" placeholder="Year built" onkeypress="javascript:return digitOnly(event)" />
                        </div>

                        <div class="form-group col-md-6">
                            <label class="control-label">Sq Fit</label>
                            <input id="sq_fit" name="sq_fit" type="text" class="form-control" placeholder="Sq Fit" onkeypress="javascript:return digitOnly(event)" />
                        </div>

                        <div class="form-group col-md-6">
                            <label class="control-label">Number limits</label>
                            <input id="number_limit" name="number_limit" type="text" class="form-control" placeholder="Number limits" onkeypress="javascript:return digitOnly(event)" />
                        </div>

                        <!--<button class="btn btn-primary nextBtn pull-right" type="button">Next</button>-->

                        <nav>
                            <ul class="pager">
                                <li class="previous"><a class="prevBtn" href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                                <li class="next"><a class="nextBtn" href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                            </ul>
                        </nav>

                    </div>
                </div>

                <div class="panel panel-primary setup-content" id="step-3" style="margin:5% 0%;border:none;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Documents</h3>
                    </div>
                    <div class="panel-body">

                        <div class="form-group col-md-12">
                            <label class="control-label">Documents</label>
                            <input id="image" type="file" required="required" class="form-control file-styled prop_image" />
                        </div>

                        <div class="col-md-12 images"></div>

                        <div class="col-md-12">&nbsp;</div>
                            <div class="col-md-12 selections"></div>
                        <div class="col-md-12">&nbsp;</div>

                        <!--<button class="btn btn-primary nextBtn pull-right" type="button">Next</button>-->

                        <nav>
                            <ul class="pager">
                                <li class="previous"><a class="prevBtn" href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                                <li class="next"><a class="nextBtn" href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
                            </ul>
                        </nav>

                    </div>
                </div>

                <div class="panel panel-primary setup-content" id="step-4" style="margin:5% 0%;border:none;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Borrower</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group col-md-3">
                            <label class="control-label">Name</label>
                            <input id="name" name="name" type="text" class="form-control" placeholder="Name" />
                        </div>

                        <div class="form-group col-md-3">
                            <label class="control-label">Email</label>
                            <input id="email" name="email" type="text" class="form-control" placeholder="Email" />
                        </div>

                        <div class="form-group col-md-3">
                            <label class="control-label">Phone</label>
                            <input id="phone" name="phone" type="text" class="form-control" placeholder="Phone" />
                        </div>

                        <div class="form-group col-md-3">
                            <label class="control-label">Postal code</label>
                            <input id="postal_code" name="postal_code" type="text" class="suggest form-control" placeholder="Postal code" onkeypress="javascript:return digitOnly(event)" />
                        </div>

                        <div class="form-group col-md-12">
                            <label class="control-label">Address</label>
                            <input id="bo_address" name="bo_address" type="text" class="form-control" placeholder="Address" />
                        </div>

                        <div class="form-group col-md-6">
                            <label class="control-label">Date of birth</label>
                            <input id="dob" name="dob" type="text" class="form-control" placeholder="DOB" />
                        </div>

                        <div class="form-group col-md-6">
                            <label for="sel1">Credit score</label>
                            <select title="credit_score" name="credit_score" class="form-control" id="credit_score">
                                <option value="">-- Select credit score --</option>
                                <option value="Excellent (720+)">Excellent (720+)</option>
                                <option value="Good (620-720)">Good (620-720)</option>
                                <option value="Fair (600-659)">Fair (600-659)</option>
                                <option value="Average (550-599)">Average (550-599)</option>
                                <option value="Poor (< 550)">Poor (< 550)</option>
                            </select>
                        </div>
                        <!--<button class="btn btn-success pull-right" type="submit">SUBMIT!</button>-->

                        <nav>
                            <ul class="pager">
                                <li class="previous"><a class="prevBtn" href="#"><span aria-hidden="true">&larr;</span> Previous</a></li>
                                <li class="next"><button id="send-loan-request" class="btn btn-success pull-right" type="button">SUBMIT!</button></li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </form>
        </div>

    </div>
@endsection

@section('js')
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/frontend/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/frontend/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/frontend/js/menumaker.js"></script>
    <!-- animsition -->
    <script type="text/javascript" src="/frontend/js/animsition.js"></script>
    <script type="text/javascript" src="/frontend/js/animsition-script.js"></script>
    <!-- sticky header -->
    <script type="text/javascript" src="/frontend/js/jquery.sticky.js"></script>
    <script type="text/javascript" src="/frontend/js/sticky-header.js"></script>
    <!-- Back to top script -->
    <script src="/frontend/js/back-to-top.js" type="text/javascript"></script>

    <!-- datepicker -->
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!-- datepicker -->

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="/frontend/js/validation/jquery.validate.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

    <script>

        $("#phone").mask("(999) 999-9999");

        @if(Session::has('error'))
            snackBarError('{{ Session::get('error') }}');
        @endif

        function digitOnly(evt)
        {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode < 48 || iKeyCode > 57)
                return false;

            return true;
        }

        $(document).ready(function () {
            var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);
                var nextStepWizard = $(this).text();

                if(nextStepWizard == 1)
                    $('.stepwizard .progress-bar').animate({width:'0%'},0);
                if(nextStepWizard == 2)
                    $('.stepwizard .progress-bar').animate({width:'33%'},0);
                if(nextStepWizard == 3)
                    $('.stepwizard .progress-bar').animate({width:'66%'},0);
                if(nextStepWizard == 4)
                    $('.stepwizard .progress-bar').animate({width:'100%'},0);


                if (!$item.hasClass('disabled')) {
                    if ($("#loanForm").valid()) {
                        navListItems.removeClass('btn-success').addClass('btn-default');
                        //navListItems.addClass('btn-default');
                        $item.addClass('btn-success');
                        allWells.hide();
                        $target.show();
                        $target.find('input:eq(0)').focus();
                    }
                }
            });

            allNextBtn.click(function () {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

//                if (isValid) {
//                    nextStepWizard.removeAttr('disabled').trigger('click');
//                }

                if($("#loanForm").valid())
                {
                    nextStepWizard.removeAttr('disabled').trigger('click');
                }
            });

            allPrevBtn.click(function () {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

                prevStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-success').trigger('click');
        });

        $("#loanForm").validate({
            rules: {
                property_type: {
                    required: true
                },
                loan_type : {
                    required : true
                },
                loan_term : {
                    required : true
                },
                amount_of_loan_request : {
                    required : true,
                    number : true
                },
                ask : {
                    required : true
                },
                downpayment_percentage : {
                    required : true,
                    number : true
                },
                net_amount : {
                    required : true,
                    number : true
                },
                address : {
                    required : true
                },
                current_value : {
                    required : true,
                    number : true
                },
                expected_value : {
                    required : true,
                    number : true
                },
                year_built : {
                    required : true,
                    number : true
                },
                sq_fit : {
                    required : true,
                    number : true
                },
                number_limit : {
                    required : true,
                    number : true
                },
                name : {
                    required : true
                },
                email : {
                    required : true,
                    email : true
                },
                phone : {
                    required : true
                },
                postal_code : {
                    required : true,
                    number : true
                },
                bo_address : {
                    required : true
                },
                dob : {
                    required : true
                },
                credit_score : {
                    required : true,
                }
            },
            messages: {
                property_type: {
                    required : "Field is required"
                },
                loan_type: {
                    required : "Field is required"
                },
                loan_term: {
                    required : "Field is required"
                },
                amount_of_loan_request : {
                    required : "Field is required",
                    number : "Please enter valid price"
                },
                ask : {
                    required : "Field is required"
                },
                downpayment_percentage : {
                    required : "Field is required",
                    number : "Please enter valid price"
                },
                net_amount : {
                    required : "Field is required",
                    number : "Please enter valid price"
                },
                address : {
                    required : "Field is required",
                },
                current_value : {
                    required : "Field is required",
                    number : "Please enter valid price"
                },
                expected_value : {
                    required : "Field is required",
                    number : "Please enter valid price"
                },
                year_built : {
                    required : "Field is required",
                    number : "Please enter valid price"
                },
                sq_fit : {
                    required : "Field is required",
                    number : "Please enter valid price"
                },
                number_limit : {
                    required : "Field is required",
                    number : "Please enter valid price"
                },
                name : {
                    required : "Field is required",
                },
                email : {
                    required : "Field is required",
                    email : "Please enter valid email"
                },
                phone : {
                    required : "Field is required"
                },
                postal_code : {
                    required : "Field is required",
                    number : "Please enter valid phone"
                },
                bo_address : {
                    required : "Field is required"
                },
                dob : {
                    required : "Field is required"
                },
                credit_score : {
                    required : "Field is required"
                }
            }
        });

        $('body').on('click','#send-loan-request',function(){
            if($("#loanForm").valid())
            {
                var imageArray = [];
                $('.prop-img').each(function() {
                    var fileName = $(this).attr('data-file-name');
                    var originalUrl = $(this).attr('data-file-original');
                    var mediumUrl = $(this).attr('data-file-medium');
                    var thumbUrl = $(this).attr('data-file-thumb');
                    var types = $(this).attr('data-types');
                    imageArray.push({
                        'fileName' : fileName,
                        'originalUrl' : originalUrl,
                        'mediumUrl' : mediumUrl,
                        'thumbUrl' : thumbUrl,
                        'types' : types
                    });
                });

                var loan_type = $('#loan_type').val();
                var property_type = $('#property_type').val();
                var purchase_price = $('#purchase_price').val();
                var amount_of_loan_request = $('#amount_of_loan_request').val();
                var loan_term = $('#loan_term').val();
                var ask = $('#ask').val();
                var downpayment_percentage = $('#downpayment_percentage').val();
                var net_amount = $('#net_amount').val();
                var address = $('#address').val();
                var current_value = $('#current_value').val();
                var expected_value = $('#expected_value').val();
                var year_built = $('#year_built').val();
                var sq_fit = $('#sq_fit').val();
                var number_limit = $('#number_limit').val();
                var bo_name = $('#name').val();
                var bo_email = $('#email').val();
                var bo_phone = $('#phone').val();
                var bo_postal_code = $('#postal_code').val();
                var bo_address = $('#bo_address').val();
                var bo_dob = $('#dob').val();
                var credit_score = $('#credit_score').val();

                var extra = [];
                $('.selection').each(function () {
                    var name = $(this).attr('data-name');
                    var value = $(this).val().trim();
                    extra.push({
                        'question' : name,
                        'answer' : value
                    });
                });

//                console.log(extra);

                showLoading();

                $.ajax({
                    type: "POST",
                    url: '{{ route('frontend.api.loan.sendLoanRequest') }}',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'bo_name' : bo_name,
                        'bo_email' : bo_email,
                        'bo_phone' : bo_phone,
                        'bo_address' : bo_address,
                        'bo_postal_code' : bo_postal_code,
                        'bo_dob' : bo_dob,
                        'credit_score' : credit_score,
                        'address' : address,
                        'current_value' : current_value,
                        'expected_value' : expected_value,
                        'year_built' : year_built,
                        'sq_fit' : sq_fit,
                        'number_limits' : number_limit,
                        'loan_type' : loan_type ,
                        'property_type' : property_type ,
                        'purchase_price' : purchase_price,
                        'amount_of_loan_request' : amount_of_loan_request,
                        'loan_terms' : loan_term,
                        'ask' : ask,
                        'dp_percentage' : downpayment_percentage,
                        'net_amount' : net_amount,
                        'imageArray' : imageArray,
                        'extra' : extra
                    },
                    async: true,
                    dataType: "json",
                    success: function (data) {
                        hideLoading();
                        if(data.status == true)
                        {
                            snackBarSuccess(data.message);
                            setTimeout(function () {
                                //window.location.href = "{{ route('frontend.loan.requestList') }}";
                            },1000);
                        }
                        else
                        {
                            snackBarError(data.message);
                        }
                    },error:function () {
                        hideLoading();
                        snackBarError('something went wrong');
                    }
                });
                return false;
            }
            return false;
        });

        $('#dob').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: "d-m-yy"
        });

        $('body').on('change','#downpayment_percentage',function(){
            if($('#amount_of_loan_request').val().trim() == null || $('#amount_of_loan_request').val().trim() == '')
            {
                snackBarError('Please enter Amount of loan request');
            }
            var loan_req_amt = $('#amount_of_loan_request').val();
            var dp_amt = loan_req_amt * $(this).val() / 100;
            var net_amt = loan_req_amt - dp_amt;
            $('#net_amount').val(net_amt);
        });

        var clicks = 0;
        $("body").on("change",".prop_image",function() {

            var ext = this.value.match(/\.(.+)$/)[1];

            if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'pdf' || ext == 'PDF')
            {
                showLoading();
                var html = '';
                var file_data = $(this).prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('_token', '{{ csrf_token() }}');
                $.ajax({
                    url: '{{ route('frontend.api.image.attachDoc') }}', // point to server-side PHP script
                    dataType: 'json',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(data){
                        hideLoading();
                        if(data.status == true)
                        {
                            var uurl = ''; var types = 1;
                            if(ext == 'pdf' || ext == 'PDF')
                            {
                                types = 2;
                                uurl = '/frontend/pdf.png';
                            }
                            else
                            {
                                uurl = data.medium_url;
                            }
                            html +='<div class="col-md-3 prop-img prop-images'+clicks+'" data-file-name="'+data.image_name+'" data-file-original="'+data.original_url+'" data-file-medium="'+data.medium_url+'" data-file-thumb="'+data.thumb_url+'" data-types="'+types+'" style="margin-bottom: 3%;">';
                            html +='<img style="margin-left:0%;box-shadow:0px 0px 2px gray;padding:5px;max-height:150px;max-width: 150px;" src="'+uurl+'" height="150" width="150"/>';
                            html +='<span style="margin-left:2%;margin-right:2%;margin-top:-40%;cursor: pointer;background-color: #E36758;" data-class="prop-images'+clicks+'" data-file-name="'+data.image_name+'" data-file-original="'+data.original_url+'" data-file-medium="'+data.medium_url+'" data-file-thumb="'+data.thumb_url+'" class="remove_image label bg-danger" title="Remove"><i class="fa fa-trash-o"></i></span>';
                            html +='</div>';
                            $(".images").append(html);
                            clicks++;
                        }
                        else
                        {
                            snackBarError('Something went wrong. please try again !');
                        }
                    },error:function(error){
                        hideLoading();
                    }
                });
                return false;
            }
            else
            {
                hideLoading();
                snackBarError('Something went wrong. please try again !');
            }
        });

        $("body").on("click",".remove_image",function() {
            var singleClass = $(this).attr('data-class');
            $("." + singleClass).remove();
        });

        $('body').on('change','#loan_type',function(){
            $.ajax({
                type: "GET",
                url: '{{ route('frontend.api.question.getQuestionsFormLoanType') }}',
                data: {
                    'loan_type_id': $(this).val()
                },
                async: true,
                dataType: "json",
                success: function (data) {
                    var selections = '';
                    if(data.questions.length > 0)
                    {
                        $.each(data.questions,function(index,value) {

                            selections += '<div class="col-md-6">';
                            selections += '                <h5>'+value.question+' (Optional)</h5>';
                            selections += '                <div class="select-input disabled-first-option">';
                            if(value.input_type == 1)
                            {
                                selections += '<input type="text" data-name="'+value.question+'" name="selection" class="selection form-control" placeholder="'+value.question+'">';
                            }
                            else
                            {
                                selections += '<textarea type="text" data-name="'+value.question+'" name="selection" class="selection form-control" placeholder="'+value.question+'"></textarea>';
                            }

                            selections += '                </div>';
                            selections += '            </div>';


                        });
                        $('.selections').html(selections);
                    }
                    else
                    {
                        $('.selections').html('');
                    }

                },error:function(error){
                    alert('in error portion');
                }
            });
            return false;
        });
    </script>

@endsection