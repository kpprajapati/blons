@extends('frontend.main')

@section('title')
    Loan request
@endsection

@section('css')
@endsection

@section('content')
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Loan Product</a></li>
                        <li class="active">{{ $inc->loanType->name }}</li>
                    </ol>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="bg-white pinside30">
                    <div class="row">
                        <div class="col-md-5 col-sm-4 col-xs-12">
                            <h1 class="page-title">{{ $inc->loanType->name }}</h1>
                        </div>
                        <div class="col-md-7 col-sm-8 col-xs-12">
                            <div class="row">
                                <div class="col-md-12 hidden-sm hidden-xs">
                                    <div class="rate-block">
                                        <h1 class="rate-number">{{ $inc->dp_percentage }}%</h1>
                                        <small>Down payment %</small> </div>
                                </div>
                                {{--<div class="col-md-8 col-sm-12 col-xs-12">--}}
                                    {{--<div class="btn-action"> <a href="contact-us.html" class="btn btn-primary">Get a call back</a> <a href="#" class="btn btn-default">Emi Calculator</a> </div>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sub-nav" id="sub-nav">
                    <ul class="nav nav-justified">
                        <li><a href="#loanDetail" class="page-scroll">Loan details</a></li>
                        <li><a href="#loanInfo" class="page-scroll">Loan Information</a></li>
                        <li><a href="#attachment" class="page-scroll">Attachments</a></li>
                        <li><a href="#extraInfo" class="page-scroll">Extra information</a></li>
                        <li><a href="#borrower" class="page-scroll">Borrower</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=" ">
    <!-- content start -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="wrapper-content bg-white">
                    <div class="section-scroll pinside60" id="loanDetail">
                        <h1>Loan details</h1>
                        <div class="col-md-12" style="padding-top: 2%;">
                            <div class="form-group col-md-4">
                                <label class="sel1">Property type</label>
                                <input title="" name="" type="text" class="form-control" value="{{ $inc->propertyType->name }}" disabled="disabled"/>
                            </div>

                            <div class="form-group col-md-4">
                                <label class="sel1">Loan type</label>
                                <input title="" name="" type="text" class="form-control" value="{{ $inc->loanType->name }}" disabled="disabled"/>
                            </div>

                            <div class="form-group col-md-4">
                                <label class="sel1">Loan terms</label>
                                <input title="" name="" type="text" class="form-control" value="{{ $inc->loan_terms }}" disabled="disabled"/>
                            </div>

                            <div class="form-group col-md-3">
                                <label class="sel1">Purchase price</label>
                                <input title="" name="" type="text" class="form-control" value="{{ $inc->purchase_price }}" disabled="disabled"/>
                            </div>

                            <div class="form-group col-md-3">
                                <label class="sel1">Amount of loan request</label>
                                <input title="" name="" type="text" class="form-control" value="{{ $inc->amount_of_loan_request }}" disabled="disabled"/>
                            </div>

                            <div class="form-group col-md-3">
                                <label class="sel1">Down payment(%)</label>
                                <input title="" name="" type="text" class="form-control" value="{{ $inc->dp_percentage }}%" disabled="disabled"/>
                            </div>

                            <div class="form-group col-md-3">
                                <label class="sel1">Net amount</label>
                                <input title="" name="" type="text" class="form-control" value="{{ $inc->net_amount }}" disabled="disabled"/>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="sel1">Net amount</label>
                                <textarea title="" name="" class="form-control" cols="" rows="5" disabled="disabled">{{ $inc->ask }}</textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="section-scroll" id="loanInfo">
                        <div class="bg-light pinside60">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="section-title mb60">
                                        <h1>Loan Information</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group col-md-4">
                                        <label class="sel1">Year built</label>
                                        <input title="" name="" type="text" class="form-control" value="{{ $inc->year_built }}" disabled="disabled"/>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label class="sel1">Sq. Fit</label>
                                        <input title="" name="" type="text" class="form-control" value="{{ $inc->sq_fit }}" disabled="disabled"/>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label class="sel1">Number limit</label>
                                        <input title="" name="" type="text" class="form-control" value="{{ $inc->number_limits }}" disabled="disabled"/>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label class="sel1">Current value</label>
                                        <input title="" name="" type="text" class="form-control" value="{{ $inc->current_value }}" disabled="disabled"/>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label class="sel1">Expected value</label>
                                        <input title="" name="" type="text" class="form-control" value="{{ $inc->expected_value }}" disabled="disabled"/>
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div class="section-scroll pinside60" id="attachment">
                        <h1>Attachments</h1>
                        <div class="col-md-12" style="padding-top: 2%;">
                            @foreach($inc->attachments as $index => $doc)
                                <div class="form-group col-md-4">
                                    @if($doc->doc_type == \App\Models\Attachment::IMAGE)
                                        <img src="{{ $doc->medium_url }}" style="box-shadow: 0px 0px 2px gray;padding:5px;"/>
                                        @else
                                        <a href="{{ $doc->medium_url }}" target="_blank">
                                            <img src="/frontend/pdf.png" style="box-shadow: 0px 0px 2px gray;padding:5px;"/>
                                        </a>
                                    @endif
                                </div>
                            @endforeach

                        </div>
                        <div class="clearfix"></div>
                    </div>


                    <div class="section-scroll" id="extraInfo">
                        <div class=" bg-light pinside60">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="section-title mb60">
                                        <h1>Extra information</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                @foreach($inc->extraInformation as $index => $info)
                                    <div class="form-group col-md-4">
                                        <label class="sel1">{{ $info->question }}</label>
                                        <input title="" name="" type="text" class="form-control" value="{{ $info->answer }}" disabled="disabled"/>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12"> </div>
                            </div>
                        </div>
                    </div>

                        <div class="section-scroll pinside60" id="borrower">
                            <h1>Borrower</h1>
                            <div class="col-md-12" style="padding-top: 2%;">
                                <div class="form-group col-md-4">
                                    <label class="sel1">Name</label>
                                    <input title="" name="" type="text" class="form-control" value="{{ $inc->bo_name }}" disabled="disabled"/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="sel1">Email</label>
                                    <input title="" name="" type="text" class="form-control" value="{{ $inc->bo_email }}" disabled="disabled"/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="sel1">Phone</label>
                                    <input title="" name="" type="text" class="form-control" value="{{ $inc->bo_phone }}" disabled="disabled"/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="sel1">Postal code</label>
                                    <input title="" name="" type="text" class="form-control" value="{{ $inc->bo_postal_code }}" disabled="disabled"/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="sel1">DOB</label>
                                    <input title="" name="" type="text" class="form-control" value="{{ $inc->bo_dob }}" disabled="disabled"/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="sel1">Credit score</label>
                                    <input title="" name="" type="text" class="form-control" value="{{ $inc->credit_score }}" disabled="disabled"/>
                                </div>

                                <div class="form-group col-md-12">
                                    <label class="sel1">Address</label>
                                    <input title="" name="" type="text" class="form-control" value="{{ $inc->bo_address }}" disabled="disabled"/>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="/frontend/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/frontend/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/frontend/js/menumaker.js"></script>
    <!-- animsition -->
    <script type="text/javascript" src="/frontend/js/animsition.js"></script>
    <script type="text/javascript" src="/frontend/js/animsition-script.js"></script>
    <!-- sticky header -->
    <script type="text/javascript" src="/frontend/js/jquery.sticky.js"></script>
    <script type="text/javascript" src="/frontend/js/sticky-header.js"></script>
    <!-- one page scroling navigation -->
    <script type="text/javascript" src="/frontend/js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="/frontend/js/scrolling-nav.js"></script>
    <!-- Faq Accordion -->
    <script src="/frontend/js/accordion.js" type="text/javascript"></script>
    <script src="/frontend/js/jquery-ui.js"></script>
    <script>
        $(function() {
            $("#slider-range-min").slider({
                range: "min",
                value: 3000,
                min: 1000,
                max: 5000,
                slide: function(event, ui) {
                    $("#amount").val("$" + ui.value);
                }
            });
            $("#amount").val("$" + $("#slider-range-min").slider("value"));
        });
    </script>
    <script>
        $(function() {
            $("#slider-range-max").slider({
                range: "max",
                min: 1,
                max: 10,
                value: 2,
                slide: function(event, ui) {
                    $("#j").val(ui.value);
                }
            });
            $("#j").val($("#slider-range-max").slider("value"));
        });
    </script>
    <!-- Back to top script -->
    <script src="/frontend/js/back-to-top.js" type="text/javascript"></script>
@endsection