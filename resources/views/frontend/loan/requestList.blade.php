@extends('frontend.main')

@section('title')
    Loan request list
@endsection

@section('css')
    <style>
        .table>thead>tr>th
        {
            padding: 5px 8px !important;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
            text-align: left !important;
        }
    </style>
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">--}}
@endsection

@section('content')
    <div class="col-md-12">&nbsp;</div>
    <div class="col-md-12">&nbsp;</div>
    <div class="col-md-12">&nbsp;</div>
    <div class="">
        <!-- content start -->

        <div class="col-md-12">
            <div class="col-md-2"></div>
            <div class="col-md-10 text-right">
                <a href="{{ route('frontend.loan.index') }}" style="margin-right:5%;color:#778191;border:1px solid #778191;padding:5px 10px;border-radius: 2px;">
                    <i class="fa fa-plus"></i> &nbsp;&nbsp;Create New
                </a>
            </div>
        </div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        {{--<div class="col-md-10" style="background: white;">--}}
        <div class="container">
            <table class="table">
                <thead class="thead-inverse">
                <tr>
                    <th>#</th>
                    <th>Request number</th>
                    <th>Loan amount</th>
                    <th>Down payment (%)</th>
                    <th>Date/Time</th>
                    <th>More detail</th>
                </tr>
                </thead>
                <tbody>

                @foreach($requests as $index => $re)
                <tr>
                    <th scope="row">{{ $index + 1 }}</th>
                    <td>{{ $re->request_number }}</td>
                    <td>${{ $re->net_amount }}</td>
                    <td>{{ $re->dp_percentage }}%</td>
                    <td>{{ \Carbon\Carbon::parse($re->created_at)->format('d M Y h:i A') }}</td>
                    <td>
                        <a href="{{ route('frontend.loan.detail',['id' => $re->id]) }}"><i class="fa fa-file-o"></i></a>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>

        <div class="col-md-12 text-center">
            {{ $requests->links() }}
        </div>
        {{--</div>--}}
        <div class="clearfix"></div>
    </div>
@endsection


@section('js')
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/frontend/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/frontend/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/frontend/js/menumaker.js"></script>
    <!-- animsition -->
    <script type="text/javascript" src="/frontend/js/animsition.js"></script>
    <script type="text/javascript" src="/frontend/js/animsition-script.js"></script>
    <!-- sticky header -->
    <script type="text/javascript" src="/frontend/js/jquery.sticky.js"></script>
    <script type="text/javascript" src="/frontend/js/sticky-header.js"></script>
    <!-- Back to top script -->
    <script src="/frontend/js/back-to-top.js" type="text/javascript"></script>

    <!-- datepicker -->
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!-- datepicker -->

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="/frontend/js/validation/jquery.validate.js"></script>

    <script>
        @if(Session::has('error'))
        snackBarError('{{ Session::get('error') }}');
        @endif
    </script>
@endsection
