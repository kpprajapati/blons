@if(Auth::user())
    @if(Auth::user()->roles)
<div class="col-md-12" style="background: #f5f5f5;z-index: 500;margin-bottom: 20px;"> {{--background: rgb(220, 220, 220);--}}
<div class="container">
    <div class="row">
        <div class="col-md-12 margin-top-10 margin-bottom-10" >

            <div class="menu-responsive">
                <i class="fa fa-reorder menu-trigger"></i>
            </div>

            <nav id="navigation" class="style-1 setmanu" style="">
                <ul id="responsive" >

                        <li><a class= "{{ Request::path() == "/" ? 'current' : ''  }}" href="{{ route('frontend.home.index') }}"><i class="fa fa-search" aria-hidden="true"></i> Search</a>
                        </li>

                    <li><a class= "{{ Request::path() == "favourite" || Request::path() == "album" || Request::path() == "album/add" ? 'current' : ''  }}" href="#"> <i class="fa fa-heart" aria-hidden="true"></i>  Favourite</a>
                        <ul>
                            <li><a class="{{ Request::path() == "favourite" ? 'current' : ''  }}" href="{{ route('frontend.favourite.index') }}">Favourite</a></li>
                            <li><a class="{{ Request::path() == "album" ? 'current' : ''  }}" href="{{ route('frontend.album.index') }}">Album</a></li>

                            @if(Auth::user()->agent)
                                <li><a class="{{ Request::path() == "album/add" ? 'current' : ''  }}" href="{{ route('frontend.album.add') }}">Create album</a></li>
                            @endif
                        </ul>
                    </li>

                    @if(Auth::user() && Auth::user()->hasRole('agent'))
                        <li><a class= "{{ Request::path() == "property/add" || Request::path() == "agentProfile " ? 'current' : ''  }}" href="#"><i class="fa fa-home" aria-hidden="true"></i> Property</a>
                            <ul>
                                <li><a class="{{ Request::path() == "property/add" ? 'current' : ''  }}" href="{{ route('frontend.property.add') }}">Add Property</a></li>
                                <li><a class="{{ Request::path() == "agentProfile" ? 'current' : ''  }}" href="{{ route('frontend.agentProfile.profile') }}#myProperty">My Property</a></li>
                            </ul>
                        </li>
                    @endif
                    <li><a class= "{{ Request::path() == "chat" ? 'current' : ''  }}" href="{{ route('frontend.chat.index') }}"><i class="fa fa-comments" aria-hidden="true"></i> Chat</a></li>

                    @if(Auth::user() && Auth::user()->hasRole('customer'))
                        <li><a class= "{{ Request::path() == "agentProfile" ? 'current' : ''  }}" href="{{ route('frontend.profile.index') }}"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                    @endif

                    @if(Auth::user() && Auth::user()->hasRole('agent'))
                        <li><a class= "{{ Request::path() == "agentProfile" ? 'current' : ''  }}" href="{{ route('frontend.agentProfile.profile') }}"> <i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                        <li><a class="{{ Request::path() == "property/offerList" ? 'current' : ''  }}" href="{{ route('frontend.offer.offerList') }}"> <i class="fa fa-gift" aria-hidden="true"></i> Offers</a></li>
                    @endif

                    <li><a class="{{ Request::path() == "invite" ? 'current' : ''  }}" href="{{ route('frontend.invite.index') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> Invite</a></li>

                    @if(Auth::user()->hasRole('customer'))
                        <li><a class="{{ Request::path() == "addPropertyRequest" ? 'current' : ''  }}" href="{{ route('frontend.property.addPropertyRequest') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> Property request</a></li>
                    @endif

                </ul>
            </nav>

            <div class="clearfix"></div>

        </div>
    </div>
</div>
</div>
@endif
@endif
