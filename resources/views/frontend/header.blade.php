<div class="collapse searchbar" id="searchbar">
    <div class="search-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span> </div>
                    <!-- /input-group -->
                </div>
                <!-- /.col-lg-6 -->
            </div>
        </div>
    </div>
</div>

<div class="top-bar">
    <!-- top-bar -->
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-xs hidden-sm">
                <p class="mail-text">Welcome to our Basrow funding</p>
            </div>
            <div class="col-md-8 col-sm-12 text-right col-xs-12">
                <div class="top-nav">
                    {{--<span class="top-text hidden-xs"><a href="#">View Locations</a> </span> --}}
                    <span class="top-text"><a href="#">+1800-123-4567</a></span>

                    @if(Auth::user())
                        <span class="top-text"><a href="#"><i class="fa fa-user"></i>&nbsp; {{ Auth::user()->name }}</a></span>
                        <span class="top-text"><a href="{{ route('frontend.login.logout') }}"><i class="fa fa-power-off"></i> Logout</a></span>
                        @else
                        <span class="top-text"><a href="{{ route('frontend.login.index') }}"><i class="fa fa-user"></i>&nbsp; Sign In</a></span>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

<div id="sticky-wrapper" class="sticky-wrapper" style="height: 100px;"><div class="header" style="width: 1286px;">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-12 col-xs-6">
                    <!-- logo -->
                    <div class="logo">
                        <a href="{{ route('frontend.home.index') }}"><img src="/frontend/images/logo.png" alt="Borrow - Loan Company Website Template"></a>
                    </div>
                </div>
                <!-- logo -->
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div id="navigation"><div id="menu-button">Menu</div>
                        <!-- navigation start-->
                        <ul>
                            <li class="active">
                                {{--<span class="submenu-button"></span>--}}
                                <a href="{{ route('frontend.home.index') }}" class="animsition-link">Home</a>
                                {{--<ul>--}}
                                    {{--<li><a href="index.html" title="Home page 1" class="animsition-link">Home page 1</a></li>--}}
                                    {{--<li><a href="index-1.html" title="Home page 2" class="animsition-link">Home page 2</a></li>--}}
                                    {{--<li><a href="index-2.html" title="Home page 3" class="animsition-link">Home page 3</a></li>--}}
                                    {{--<li><a href="index-3.html" title="Home page 4" class="animsition-link">Home page 4--}}
                                            {{--<span class="badge">New</span></a></li>--}}
                                    {{--<li><a href="index-4-students-loan.html" title="students loan" class="animsition-link">Students Loan Page <span class="badge">New</span></a> </li>--}}
                                    {{--<li><a href="index-5-business-loan.html" title="students loan" class="animsition-link">Business Loan Page<span class="badge">New</span></a></li>--}}
                                {{--</ul>--}}
                            </li>
                            @if(Auth::user())
                                <li><a href="{{ route('frontend.loan.requestList') }}" title="Loan request" class="animsition-link">Loan request list</a></li>
                            @endif
                        </ul>
                    </div>
                    <!-- /.navigation start-->
                </div>
                {{--<div class="col-md-1 hidden-sm">--}}
                    {{--<!-- search start-->--}}
                    {{--<div class="search-nav"> <a class="search-btn" role="button" data-toggle="collapse" href="#searchbar" aria-expanded="false"><i class="fa fa-search"></i></a> </div>--}}
                {{--</div>--}}
                <!-- /.search start-->
            </div>
        </div>
    </div></div>