@extends('admin.main')

@section('title')
    Inquiries
@endsection

@section('head-js')
    <script type="text/javascript" src="/backend/material/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/forms/selects/select2.min.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/pages/datatables_sorting.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/core/app.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/ui/ripple.min.js"></script>

@endsection

@section('content')


    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-left52 position-left"></i>
                    <span class="text-semibold">Inquiries</span> - list
                    <small class="display-block">List of all inquiries !</small>
                </h4>
            </div>


        </div>
    </div>
    <!-- /page header -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page length options -->
                <div class="panel panel-flat">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Loan Amount</th>
                            <th>Down payment (%)</th>
                            <th>Time</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($inquiries as $index => $inquiry)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>&nbsp;{{ $inquiry->bo_name }}</td>
                                <td>{{ $inquiry->bo_email }}</td>
                                <td>{{ $inquiry->bo_phone }}</td>
                                <td>${{ $inquiry->net_amount }}</td>
                                <td>${{ $inquiry->dp_percentage }}%</td>
                                <td>{{ \Carbon\Carbon::parse($inquiry->created_at)->format('d M Y h:i A') }}</td>
                                <td class="">
                                    <a href="{{ route('admin.inquiry.detail',['id' => $inquiry->id ]) }}">
                                        <i class="fa fa-file"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>



                    {{--<div class="panel-body">--}}
                    {{--It is possible to easily customise the options shown in the length menu using the <code>lengthMenu</code> initialisation option. This parameter can take one of two forms: 1) A <code>1D</code> array of options which will be used for both the displayed option and the value; 2) A <code>2D</code> array in which the first array is used to define the value options and the second array the displayed options. The example below shows a 2D array being used to include a <code>"Show all"</code> records option.--}}
                    {{--</div>--}}

                </div>
                <div class="col-ms-12">{{ $inquiries->links() }}</div>
                <!-- /page length options -->
            </div>
        </div>
    </div>

    <!-- Basic modal -->
    <div id="modal_default" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Request details</h5>
                </div>

                <div class="modal-body">
                    <div class="col-md-12 mo-body">

                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
        </div>
    </div>
    <!-- /basic modal -->
@endsection

@section('js')
    {{--<script>--}}
        {{--$('body').on('click','.view',function () {--}}
            {{--var html = '';--}}
            {{--html +='<div class="form-group col-md-12">';--}}
            {{--html +='    <label>Name:</label>';--}}
            {{--html +='<input type="text" class="form-control" placeholder="Eugene Kopyov" value="'+$(this).attr('data-name')+'">';--}}
            {{--html +='    </div>';--}}

            {{--html +='<div class="form-group col-md-6">';--}}
            {{--html +='    <label>Email:</label>';--}}
            {{--html +='<input type="text" class="form-control" placeholder="Eugene Kopyov" value="'+$(this).attr('data-email')+'">';--}}
            {{--html +='    </div>';--}}

            {{--html +='<div class="form-group col-md-6">';--}}
            {{--html +='    <label>Phone:</label>';--}}
            {{--html +='<input type="text" class="form-control" placeholder="Eugene Kopyov" value="'+$(this).attr('data-phone')+'">';--}}
            {{--html +='    </div>';--}}

            {{--html +='<div class="form-group col-md-6">';--}}
            {{--html +='    <label>Loan type:</label>';--}}
            {{--html +='<input type="text" class="form-control" placeholder="Eugene Kopyov" value="'+$(this).attr('data-loan-type')+'">';--}}
            {{--html +='    </div>';--}}

            {{--html +='<div class="form-group col-md-6">';--}}
            {{--html +='    <label>Loan amount:</label>';--}}
            {{--html +='<input type="text" class="form-control" placeholder="Eugene Kopyov" value="'+$(this).attr('data-loan-amount')+'">';--}}
            {{--html +='    </div>';--}}

            {{--html +='<div class="form-group col-md-12">';--}}
            {{--html +='    <label>Description</label>';--}}
            {{--html +='<textarea rows="5" cols="5" class="form-control" placeholder="">'+$(this).attr('data-description')+'</textarea>';--}}
            {{--html +='    </div>';--}}

            {{--html +='<div class="attachment-div"></div>';--}}

            {{--$('#modal_default .mo-body').html(html);--}}

            {{--$('#modal_default').modal();--}}

            {{--$.ajax({--}}
                {{--method : 'GET',--}}
                {{--data : {--}}
                    {{--'inquiry_id' : $(this).attr('data-id')--}}
                {{--},--}}
                {{--type : 'json',--}}
                {{--url : '{{ route('admin.inquiry.getAttachment') }}',--}}
                {{--success : function (data) {--}}
                    {{--var html1 = '';--}}
                    {{--$.each(data.attachment,function(index,value){--}}
                        {{--html1 +='<div class="col-md-3">';--}}
                        {{--if(value.doc_type == 2)--}}
                        {{--{--}}
                            {{--html1 +='<a target="_blank" href="'+value.medium_url+'"><img src="/frontend/pdf.png" height="100" width="100" style="box-shadow: 0px 0px 2px gray;padding:5px;"/></a>';--}}
                        {{--}--}}
                        {{--else--}}
                        {{--{--}}
                            {{--html1 +='<img src="'+value.medium_url+'" height="100" width="100" style="box-shadow: 0px 0px 2px gray;padding:5px;"/>';--}}
                        {{--}--}}
                        {{--html1 +='    </div>';--}}

                    {{--});--}}
                    {{--$('.attachment-div').html(html1);--}}
                {{--},error : function (error) {--}}

                {{--}--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@endsection