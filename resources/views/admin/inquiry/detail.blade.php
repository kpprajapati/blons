@extends('admin.main')

@section('title')
    User inquiry
@endsection

@section('head-js')
    <script type="text/javascript" src="/backend/material/assets/js/core/libraries/jasny_bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/visualization/echarts/echarts.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/pages/gallery.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/core/libraries/jquery_ui/interactions.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/core/libraries/jquery_ui/touch.min.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/core/app.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/pages/components_navs.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/pages/user_profile_tabbed.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')


    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-left52 position-left"></i>
                    <span class="text-semibold">{{ $inc->user->name }}</span> - Inquiry
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{{ route('admin.inquiry.index') }}" class="btn btn-link btn-float has-text text-size-small"><i class="icon-menu6 text-indigo-400"></i><span>List</span></a>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-default sidebar-separate">
                <div class="sidebar-content">

                    <!-- User details -->
                    <div class="content-group">
                        <div class="panel-body bg-indigo-400 border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
                            <div class="content-group-sm">
                                <h6 class="text-semibold no-margin-bottom">
                                    {{ $inc->user->name }}
                                </h6>

                                {{--<span class="display-block">Agent</span>--}}
                            </div>

                            <a href="#" class="display-inline-block content-group-sm">
                                    <img src="/backend/material/assets/images/placeholder.jpg" class="img-circle img-responsive" alt="" style="width: 110px; height: 110px;">
                            </a>
                            {{--<ul class="list-inline list-inline-condensed no-margin-bottom">--}}
                            {{--<li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-google-drive"></i></a></li>--}}
                            {{--<li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-twitter"></i></a></li>--}}
                            {{--<li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-github"></i></a></li>--}}
                            {{--</ul>--}}
                        </div>

                        <div class="panel no-border-top no-border-radius-top">
                            <ul class="navigation">
                                <li class="navigation-header">Navigation</li>
                                <li class="active"><a href="#profile" data-toggle="tab"><i class="icon-files-empty"></i> Inquiry</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /user details -->
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Tab content -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="profile">
                        <!-- Profile info -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h6 class="panel-title">Inquiry information</h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <form action="#">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Request Number</label>
                                                <input title="" type="text" value="{{ $inc->request_number }}" class="form-control" disabled="disabled">
                                            </div>
                                            <div class="col-md-6">
                                                <label>Borrower name</label>
                                                <input title="" type="text" value="{{ $inc->bo_name }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Borrower email</label>
                                                <input title="" type="text" value="{{ $inc->bo_email }}" class="form-control" disabled>
                                            </div>

                                            <div class="col-md-6">
                                                <label>Borrower phone</label>
                                                <input title="" type="text" value="{{ $inc->bo_phone }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    {{--<div class="col-md-12">--}}
                                        {{--<h3 class="title text-center">Address</h3>--}}
                                    {{--</div>--}}

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Borroer Address</label>
                                                <input title="" type="text" value="{{ $inc->bo_address }}" class="form-control" disabled="disabled">
                                            </div>
                                            <div class="col-md-12">&nbsp;</div>
                                            <div class="col-md-4">
                                                <label>Borrower Postal code</label>
                                                <input title="" type="text" value="{{ $inc->bo_postal_code }}" class="form-control" disabled>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Borrower DOB</label>
                                                <input title="" type="text" value="{{ $inc->bo_dob }}" class="form-control" disabled>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Credit score</label>
                                                <input title="" type="text" value="{{ $inc->credit_score }}" class="form-control" disabled>
                                            </div>
                                            <div class="col-md-12">&nbsp;</div>

                                            <div class="col-md-12">
                                                <label>Address</label>
                                                <input title="" type="text" value="{{ $inc->address }}" class="form-control" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Property type</label>
                                                <input title="" type="text" value="{{ $inc->propertyType->name }}" class="form-control" disabled="disabled">
                                            </div>
                                            <div class="col-md-6">
                                                <label>Loan type</label>
                                                <input title="" type="text" value="{{ $inc->loanType->name }}" class="form-control" disabled="disabled">
                                            </div>

                                            <div class="col-md-12">&nbsp;</div>
                                            <div class="col-md-3">
                                                <label>Current value</label>
                                                <input title="" type="text" value="{{ $inc->current_value }}" class="form-control" disabled="disabled">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Expected value</label>
                                                <input title="" type="text" value="{{ $inc->expected_value }}" class="form-control" disabled="disabled">
                                            </div>

                                            <div class="col-md-12">&nbsp;</div>

                                            <div class="col-md-3">
                                                <label>Year built</label>
                                                <input title="" type="text" value="{{ $inc->year_built }}" class="form-control" disabled="disabled">
                                            </div>

                                            <div class="col-md-3">
                                                <label>Sq. fit</label>
                                                <input title="" type="text" value="{{ $inc->sq_fit }}" class="form-control" disabled="disabled">
                                            </div>

                                            <div class="col-md-3">
                                                <label>Number limits</label>
                                                <input title="" type="text" value="{{ $inc->number_limits }}" class="form-control" disabled="disabled">
                                            </div>

                                            <div class="col-md-3">
                                                <label>Purchase price</label>
                                                <input title="" type="text" value="{{ $inc->purchase_price }}" class="form-control" disabled="disabled">
                                            </div>

                                            <div class="col-md-12">&nbsp;</div>

                                            <div class="col-md-3">
                                                <label>Amount of loan request</label>
                                                <input title="" type="text" value="{{ $inc->amount_of_loan_request }}" class="form-control" disabled="disabled">
                                            </div>

                                            <div class="col-md-3">
                                                <label>Loan terms</label>
                                                <input title="" type="text" value="{{ $inc->loan_terms }}" class="form-control" disabled="disabled">
                                            </div>

                                            <div class="col-md-3">
                                                <label>Down payment (%)</label>
                                                <input title="" type="text" value="{{ $inc->dp_percentage }}" class="form-control" disabled="disabled">
                                            </div>

                                            <div class="col-md-3">
                                                <label>Net amount</label>
                                                <input title="" type="text" value="{{ $inc->net_amount}}" class="form-control" disabled="disabled">
                                            </div>

                                            <div class="col-md-12">&nbsp;</div>

                                            <div class="col-md-12">
                                                <label>Ask</label>
                                                <input title="" type="text" value="{{ $inc->ask }}" class="form-control" disabled="disabled">
                                            </div>

                                            <div class="col-md-12 text-center">&nbsp;
                                                <h3>Extra Information</h3>
                                            </div>

                                            <div class="col-md-12">&nbsp;</div>

                                            @if($inc->extraInformation)
                                                @foreach($inc->extraInformation as $index => $value)
                                                    <div class="col-md-6">
                                                        <label>{{ $value->question }}</label>
                                                        <input title="" type="text" value="{{ $value->answer }}" class="form-control" disabled="disabled">
                                                    </div>
                                                @endforeach
                                            @endif

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /profile info -->
                    </div>
                </div>
                <!-- /tab content -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


    <!-- Basic modal -->
    <div id="modal_status" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title text-center" style="border-bottom: 1px solid #e6e6e6;padding-bottom: 15px;">Change Property status</h5>
                </div>

                <div class="modal-body">

                    <div class="form-group col-md-12" style="padding-left: 10%;padding-top: 2%;">
                        {{--<label class="display-block">Status</label>--}}

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" checked="checked" value="0">
                            <span class="label bg-warning">Under screening</span>
                        </label>
                        <br>
                        <br>

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" value="1">
                            <span class="label bg-success">Active</span>
                        </label>
                        <br>
                        <br>

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" checked="checked" value="2">
                            <span class="label bg-orange">Verified</span>
                        </label>
                        <br><br>

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" checked="checked" value="3">
                            <span class="status label bg-primary">Draft</span>
                        </label>
                        <br><br>

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" checked="checked" value="4">
                            <span class="label bg-danger">Under Contract</span>
                        </label>
                        <br><br>

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" checked="checked" value="5">
                            <span class="label bg-purple">Sold</span>
                        </label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button id="save_status" type="button" class="btn btn-primary">Update status</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /basic modal -->

@endsection
