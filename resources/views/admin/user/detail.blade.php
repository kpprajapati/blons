@extends('admin.main')

@section('title')
    User Profile
@endsection

@section('head-js')
    <script type="text/javascript" src="/backend/material/assets/js/core/libraries/jasny_bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/visualization/echarts/echarts.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/pages/gallery.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/core/libraries/jquery_ui/interactions.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/core/libraries/jquery_ui/touch.min.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/core/app.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/pages/components_navs.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/pages/user_profile_tabbed.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')


    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-left52 position-left"></i>
                    <span class="text-semibold">{{ $user->name }}</span> - Profile
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{{ route('admin.user.index') }}" class="btn btn-link btn-float has-text text-size-small"><i class="icon-menu6 text-indigo-400"></i><span>List</span></a>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-default sidebar-separate">
                <div class="sidebar-content">

                    <!-- User details -->
                    <div class="content-group">
                        <div class="panel-body bg-indigo-400 border-radius-top text-center" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
                            <div class="content-group-sm">
                                <h6 class="text-semibold no-margin-bottom">
                                    {{ $user->name }}
                                </h6>

                                {{--<span class="display-block">Agent</span>--}}
                            </div>

                            <a href="#" class="display-inline-block content-group-sm">
                                    <img src="/backend/material/assets/images/placeholder.jpg" class="img-circle img-responsive" alt="" style="width: 110px; height: 110px;">
                            </a>
                            {{--<ul class="list-inline list-inline-condensed no-margin-bottom">--}}
                            {{--<li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-google-drive"></i></a></li>--}}
                            {{--<li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-twitter"></i></a></li>--}}
                            {{--<li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-github"></i></a></li>--}}
                            {{--</ul>--}}
                        </div>

                        <div class="panel no-border-top no-border-radius-top">
                            <ul class="navigation">
                                <li class="navigation-header">Navigation</li>
                                <li class="active"><a href="#profile" data-toggle="tab"><i class="icon-files-empty"></i> Inquiry list</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /user details -->
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Tab content -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="profile">
                        <!-- Profile info -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h6 class="panel-title">Inquiries</h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Loan Amount</th>
                                        <th>Down payment (%)</th>
                                        <th>Time</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($user->loanRequests as $index => $inquiry)
                                        <tr>
                                            <td>{{ $index + 1 }}</td>
                                            <td>&nbsp;{{ $inquiry->bo_name }}</td>
                                            <td>{{ $inquiry->bo_email }}</td>
                                            <td>{{ $inquiry->bo_phone }}</td>
                                            <td>${{ $inquiry->net_amount }}</td>
                                            <td>${{ $inquiry->dp_percentage }}%</td>
                                            <td>{{ \Carbon\Carbon::parse($inquiry->created_at)->format('d M Y h:i A') }}</td>
                                            <td class="">
                                                <a target="_blank" href="{{ route('admin.inquiry.detail',['id' => $inquiry->id ]) }}">
                                                    <i class="fa fa-file"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /profile info -->
                    </div>
                </div>
                <!-- /tab content -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


    <!-- Basic modal -->
    <div id="modal_status" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title text-center" style="border-bottom: 1px solid #e6e6e6;padding-bottom: 15px;">Change Property status</h5>
                </div>

                <div class="modal-body">

                    <div class="form-group col-md-12" style="padding-left: 10%;padding-top: 2%;">
                        {{--<label class="display-block">Status</label>--}}

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" checked="checked" value="0">
                            <span class="label bg-warning">Under screening</span>
                        </label>
                        <br>
                        <br>

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" value="1">
                            <span class="label bg-success">Active</span>
                        </label>
                        <br>
                        <br>

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" checked="checked" value="2">
                            <span class="label bg-orange">Verified</span>
                        </label>
                        <br><br>

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" checked="checked" value="3">
                            <span class="status label bg-primary">Draft</span>
                        </label>
                        <br><br>

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" checked="checked" value="4">
                            <span class="label bg-danger">Under Contract</span>
                        </label>
                        <br><br>

                        <label class="radio-inline">
                            <input type="radio" class="styled status" name="status" checked="checked" value="5">
                            <span class="label bg-purple">Sold</span>
                        </label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button id="save_status" type="button" class="btn btn-primary">Update status</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /basic modal -->

@endsection
