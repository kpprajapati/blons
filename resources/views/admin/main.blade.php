<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | Basrow-Loan</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/backend/material/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/backend/material/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="/backend/material/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/backend/material/assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="/backend/material/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="/backend/material/assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="/backend/material/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/ui/nicescroll.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/ui/drilldown.js"></script>
    <!-- /core JS files -->

    <script src="/backend/material/assets/js/plugins/notifications/sweet_alert.min.js"></script>

    @yield('head-js')
    <!-- Theme JS files -->
    {{--<script type="text/javascript" src="/backend/material/assets/js/core/app.js"></script>--}}
    <!-- /theme JS files -->
    @yield('css')

    <style type="text/css">
        .hover{background-color: #cc0000}
        #container{ margin:0px auto; width: 800px}
        .button {
            font-weight: bold;
            padding: 7px 9px;
            background-color: #5fcf80;
            color: #fff !important;
            font-size: 12px;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            cursor: pointer;
            text-decoration: none;
            text-shadow: 0 1px 0px rgba(0,0,0,0.15);
            border-width: 1px 1px 3px !important;
            border-style: solid;
            border-color: #3ac162;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -moz-inline-stack;
            display: inline-block;
            vertical-align: middle;
            zoom: 1;
            border-radius: 3px;
            box-sizing: border-box;
            box-shadow: 0 -1px 0 rgba(255,255,255,0.1) inset;
        }
        .authorBlock{border-top:1px solid #cc0000;}
    </style>


    <style>
        /* Absolute Center Spinner */
        .loading {
            position: fixed;
            z-index: 999;
            height: 2em;
            width: 2em;
            overflow: show;
            margin: auto;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
            content: '';
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0,0,0,0.3);
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
            /* hide "loading..." text */
            font: 0/0 a;
            color: transparent;
            text-shadow: none;
            background-color: transparent;
            border: 0;
        }

        .loading:not(:required):after {
            content: '';
            display: block;
            font-size: 10px;
            width: 1em;
            height: 1em;
            margin-top: -0.5em;
            -webkit-animation: spinner 1500ms infinite linear;
            -moz-animation: spinner 1500ms infinite linear;
            -ms-animation: spinner 1500ms infinite linear;
            -o-animation: spinner 1500ms infinite linear;
            animation: spinner 1500ms infinite linear;
            border-radius: 0.5em;
            -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
            box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
        }

        /* Animation */

        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-moz-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @-o-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .z-index-1
        {
            z-index: 1;
        }
    </style>
</head>

<body>

    <div class="loading" style="display: none;">Loading&#8230;</div>

    @include('admin.menu')

    @yield('content')

    @include('admin.footer')

    <script>
        $('document').ready(function()
        {
            $('textarea').each(function(){
                    $(this).val($(this).val().trim());
                }
            );
        });
        /** swal functions */
        @if(Session::has('success'))
        swal("Success...", "{{ Session::get('success') }}", "success");
        @endif
        @if(Session::has('warning'))
        swal("Oops...", "{{ Session::get('warning') }}", "warning");
        @endif
        @if(Session::has('error'))
        swal("Oops...", "{{ Session::get('error') }}", "error");
        @endif
        /** swal functions */

        /** loading overlay start */
        function showLoading()
        {
            $('.loading').css('display','block');
        }

        function hideLoading()
        {
            $('.loading').css('display','none');
        }
        /** loading overlay end */

        function toast (title,msg,type)
        {
            var priority = type;
            var title    = title;
            var message  = msg;
            $.toaster({ priority : priority, title : title, message : message });
        }
    </script>

    <script src="/js/jquery.toaster.js"></script> <!-- for toaster -->

    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>

    <script>
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;
//
        var pusher = new Pusher('1e73fa71a48369daa0ad', {
            cluster: 'ap2',
            encrypted: true
        });

//        var channel = pusher.subscribe('channelDemoEvent');
//        channel.bind('App\\Events\\eventTrigger', function(data) {
//            console.log(data);
//            var msg = 'Temporary message';
//            toast (title = 'Success',msg,type = 'success');
//        });

        var channel = pusher.subscribe('loanRequestChannel');
        channel.bind('App\\Events\\LoanRequestEvent', function(data) {
//            console.log(data);
            notifyBrowser('New message','Loan request','#');
        });

        function notifyBrowser(title,desc,url)
        {
            if (!Notification) {
                //console.log('Desktop notifications not available in your browser..');
                return;
            }
            if (Notification.permission !== "granted")
            {
                Notification.requestPermission();
            }
            else {
                var notification = new Notification(title, {
                    icon:'https://pbs.twimg.com/profile_images/1746144007/B_smal_reasonably_small_400x400.jpg',
                    body: desc,
                });
                // Remove the notification from Notification Center when clicked.
                notification.onclick = function () {
                    window.open(url);
                };
                // Callback function when the notification is closed.
                notification.onclose = function () {
                    //console.log('Notification closed');
                };
            }
        }
    </script>

    @yield('js')
</body>
</html>