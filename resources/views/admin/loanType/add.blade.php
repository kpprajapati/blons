@extends('admin.main')

@section('title')
    Loan type Add
@endsection

@section('head-js')
    <script type="text/javascript" src="/backend/material/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/pages/form_layouts.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/forms/styling/switch.min.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/core/app.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/pages/form_checkboxes_radios.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/ui/ripple.min.js"></script>
@endsection

@section('content')


    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-left52 position-left"></i>
                    <span class="text-semibold">Loan Type</span> - Add
                    <small class="display-block">Create new loan type !</small>
                </h4>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{{ route('admin.loanType.index') }}" class="btn btn-link btn-float has-text text-size-small"><i class="icon-menu6 text-indigo-400"></i><span>List</span></a>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Vertical form options -->
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">

                        <!-- Basic layout-->
                        <form id="form" action="{{ route('admin.loanType.create') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h5 class="panel-title">Add loan type</h5>
                                    {{--<div class="heading-elements">--}}
                                        {{--<ul class="icons-list">--}}
                                            {{--<li><a data-action="collapse"></a></li>--}}
                                            {{--<li><a data-action="reload"></a></li>--}}
                                            {{--<li><a data-action="close"></a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                </div>

                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Name:</label>
                                        <input type="text" class="form-control" placeholder="Property Type name" name="name">

                                        @foreach($errors->get('name') as $error)
                                            <label id="date-error" class="validation-error-label" for="date">
                                                {{ $error }}
                                            </label>
                                        @endforeach
                                    </div>

                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="description" rows="5" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
                                        @foreach($errors->get('description') as $error)
                                            <label id="date-error" class="validation-error-label" for="description">
                                                {{ $error }}
                                            </label>
                                        @endforeach
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="status" class="styled" checked="checked" value="1">
                                            Active
                                        </label>
                                    </div>

                                    <div class="text-right">
                                        <button id="save" type="submit" class="btn btn-primary">Add <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- /basic layout -->

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $('body').on('click','#save',function(){
            showLoading();
            $('#form').submit();
        });
    </script>
@endsection