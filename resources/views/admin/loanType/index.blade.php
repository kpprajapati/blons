@extends('admin.main')

@section('title')
    Loan Type
@endsection

@section('head-js')
    <script type="text/javascript" src="/backend/material/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/forms/selects/select2.min.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/pages/datatables_sorting.js"></script>

    <script type="text/javascript" src="/backend/material/assets/js/core/app.js"></script>
    <script type="text/javascript" src="/backend/material/assets/js/plugins/ui/ripple.min.js"></script>

@endsection

@section('content')


    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-left52 position-left"></i>
                    <span class="text-semibold">Loan Type</span> - list
                    <small class="display-block">List of all loan types !</small>
                </h4>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="{{ route('admin.loanType.add') }}" class="btn btn-link btn-float has-text text-size-small"><i class="icon-plus3 text-indigo-400"></i><span>Add new</span></a>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page length options -->
                <div class="panel panel-flat">
                    {{--<div class="panel-heading">--}}
                        {{--<h5 class="panel-title">--}}
                            {{--<a href="{{ route('admin.category.add') }}">--}}
                            {{--<button type="button" class="btn bg-teal-400 btn-labeled legitRipple">--}}
                                {{--<b>--}}
                                    {{--<i class="icon-plus3"></i>--}}
                                {{--</b> Add new--}}
                                {{--<span class="legitRipple-ripple" style="left: 18.75%; top: 57.8947%; transform: translate3d(-50%, -50%, 0px); transition-duration: 0.2s, 0.5s; width: 208.898%;"></span>--}}
                            {{--</button>--}}
                            {{--</a>--}}
                        {{--</h5>--}}
                        {{--<div class="heading-elements">--}}
                            {{--<ul class="icons-list">--}}
                                {{--<li><a data-action="collapse"></a></li>--}}
                                {{--<li><a data-action="reload"></a></li>--}}
                                {{--<li><a data-action="close"></a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <table class="table datatable-sorting">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>&nbsp;</th>
                            <th>Name</th>
                            <th>&nbsp;</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($types as $index => $type)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>&nbsp;</td>
                            <td>{{ $type->name }}</td>
                            <td>&nbsp;</td>
                            <td>
                                @if($type->status)
                                    <span class="label bg-success">Active</span>
                                    @else
                                    <span class="label bg-danger">In-Active</span>
                                @endif
                            </td>
                            <td>&nbsp;</td>
                            <td class="">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="{{ route('admin.loanType.edit',['id' => $type->id ]) }}"><i class="fa fa-edit"></i>Edit</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {{--<div class="panel-body">--}}
                        {{--It is possible to easily customise the options shown in the length menu using the <code>lengthMenu</code> initialisation option. This parameter can take one of two forms: 1) A <code>1D</code> array of options which will be used for both the displayed option and the value; 2) A <code>2D</code> array in which the first array is used to define the value options and the second array the displayed options. The example below shows a 2D array being used to include a <code>"Show all"</code> records option.--}}
                    {{--</div>--}}

                </div>
                <!-- /page length options -->
            </div>
        </div>
    </div>

@endsection