<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://bootswatch.com/darkly/bootstrap.min.css">

</head>
<body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/js/jquery.toaster.js"></script>

<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script>

    function randomToast ()
    {
        var priority = 'success';
        var title    = 'Success';
        var message  = 'It worked!';

        $.toaster({ priority : priority, title : title, message : message });
    }

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('1e73fa71a48369daa0ad', {
        cluster: 'ap2',
        encrypted: true
    });

    var channel = pusher.subscribe('channelDemoEvent');
    channel.bind('App\\Events\\eventTrigger', function(data) {
         console.log(data);
        randomToast();
    });

</script>
</body>
</html>